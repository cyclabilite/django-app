from datetime import datetime

from django.db.models import Count
from django.core.management.base import BaseCommand

from cyclab.models import Segments, VoteSegment

class Command(BaseCommand):
    # TODO SI MARCHE BIEN METTRE AVEC VOTE_CALC ?

    help = 'Recalculates the vote average, based on votes already made on segment'

    def handle(self, *args, **options):
        start = datetime.now()
        cpt = 0
        updating_instances = []

        nb_votes_all = VoteSegment.objects.all().count()
        print(f"Nombre de votes : {nb_votes_all}")
        segments_with_votes = Segments.objects.annotate(num_v=Count('votes')).filter(num_v__gte=1)
        print(f"Nombre de segments avec votes : {segments_with_votes.count()}")

        for segment in segments_with_votes:
            updated_votes = segment.update_votes_lastest(save=False)

            for vote in updated_votes:
                updating_instances.append(vote)
                cpt += 1

                if cpt % 5000 == 0:
                    print(f"- {cpt} : {datetime.now() - start}")
                    VoteSegment.objects.bulk_update(updating_instances, ['latest'])
                    updating_instances = []

        VoteSegment.objects.bulk_update(updating_instances, ['latest'])

        print(f"Nbr vote updated : {cpt}")
        print("Done")
