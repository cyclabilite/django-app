import csv

from django.core.management.base import BaseCommand, CommandError
from cyclab.models import Segments

from django.db import connection

class Command(BaseCommand):
    help = 'Generate the csv file /app/cyclab/media/export/segments-notes.csv'

    def handle(self, *args, **options):
        with open('/app/cyclab/media/export/segments-notes.csv','w') as file:
            writer = csv.writer(file)
            writer.writerow([
                "way_id", "nodestart_id", "nodeend_id", "note", "note_to",
                "note_return", "note_both"])


            with connection.cursor() as cursor:
                cursor.execute("""
SELECT
    cyclab_segments.way_id,
    cyclab_segments.nodestart_id,
    cyclab_segments.nodeend_id,
    vote_average,
    vote_average_ab,
    vote_average_ba,
    vote_average_both
FROM
	cyclab_segments
WHERE vote_number > 0""")
                for row in cursor.fetchall():
                    writer.writerow([
                        row[0],
                        row[1],
                        row[2],
                        row[3],
                        row[4],
                        row[5],
                        row[6]
                    ])

        print("Export done")