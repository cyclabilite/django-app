from django.core.management.base import BaseCommand, CommandError
from cyclab.models import Segments
from datetime import datetime
from django.db.models import Count

class Command(BaseCommand):
    help = 'Recalculates the vote average, based on votes already made on segment'

    def handle(self, *args, **options):
        start = datetime.now()
        cpt = 0
        updating_instances = []

        nb_segments_all = Segments.objects.all().count()
        print(f"Nombre total de segments : {nb_segments_all}")
        nb_segments_wo_votes = Segments.objects.annotate(num_b=Count('votes')).filter(num_b__lt=1).count()
        print(f"Nombre de segments sans votes : {nb_segments_wo_votes}")
        segments_with_votes = Segments.objects.annotate(num_v=Count('votes')).filter(num_v__gte=1)
        print(f"Nombre de segments avec votes : {segments_with_votes.count()}")

        for s in segments_with_votes:
            s.recompute()
            cpt += 1
            updating_instances.append(s)

            if cpt % 5000 == 0:
                print(f"- {cpt} : {datetime.now() - start}")
                Segments.objects.bulk_update(updating_instances, ['vote_number', 'vote_average', 'vote_average_both', 'vote_average_ab', 'vote_average_ba'])
                updating_instances = []

        Segments.objects.bulk_update(updating_instances, ['vote_number', 'vote_average', 'vote_average_both', 'vote_average_ab', 'vote_average_ba'])
