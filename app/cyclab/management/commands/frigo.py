from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from cyclab.models import SegmentsDeleted
from datetime import datetime


class Command(BaseCommand):
    help = 'Command to put all current deleted segments in the fridge and set a timestamp'

    def handle(self, *args, **options):
        updating_instances = []
        for sd in SegmentsDeleted.objects.all().filter(~Q(frigo_is_manuel=True)):
            sd.infrigo = True
            sd.frigo_datetime = datetime.now()
            updating_instances.append(sd)

        SegmentsDeleted.objects.bulk_update(updating_instances, ['infrigo', 'frigo_datetime'])
