from django.core.management.base import BaseCommand
from django.core.mail import send_mail
from django.template.loader import render_to_string

from cyclab.models import User

class Command(BaseCommand):
    help = 'Send an email to all the user to invite them to change their email'

    def handle(self, *args, **options):
        for user in User.objects.all():
            subject = "Mise à jour de l'outil de cyclabilté de l'ADAV : veuillez changer de mot de passe"
            message = render_to_string('cyclab/email_change_pwd_user.txt', { 'user': user })

            send_mail(
                subject,
                message,
                None,
                [ user.email ],
                fail_silently=False,
            )

            break
            # TODO REMOVE BREAK + TESTER EMAIL MARC