from time import time, perf_counter
from sys import stdout
from django.core.management.base import BaseCommand
import psycopg2
from cyclab.vote_default.rules_to_apply import exe_rules
from cyclab.vote_default.updater import Updater
from django.db import connection
from django.conf import settings



from cyclab.views.admin import get_trivial_transfer_vote_id, transfer_vote_from_to
from cyclab.models import SegmentsDeleted, DeletedVoteSegment, SegmentsAll, Segments, VoteSegment

class Command(BaseCommand):
    help = 'To transfer vote via CLI'

    def add_arguments(self, parser):
        parser.add_argument(
            "new_buffer_size",
            type=float,
            help="size of the buffer for new segments"
        )

        parser.add_argument(
            "del_buffer_size",
            type=float,
            help="size of the buffer for deleted segments"
        )

        parser.add_argument(
            "vote_nbr_to_handle",
            type=int,
            help="number of vote to handle"
        )

        parser.add_argument(
            "--no_recreating_tables",
            action="store_true",
            help="to unactivate the reaction of the tables"
        )

        parser.add_argument(
            "seg_in_buffer",
            type=int,
            help="number of segments within buffer"
        )



    def handle(self, *args, **options):
        new_buffer_size = options["new_buffer_size"]
        del_buffer_size = options["del_buffer_size"]
        vote_nbr_to_handle = options["vote_nbr_to_handle"]
        no_recreating_tables = options["no_recreating_tables"]
        seg_in_buffer = options["seg_in_buffer"]

        if not no_recreating_tables:
            # recreating the tables
            print("RECREATING TABLES (use --no_recreating_tables option to skip this step)")
            self.prepare_table(new_buffer_size, del_buffer_size, seg_in_buffer)
        else:
            print("NO RECREATING TABLES (option --no_recreating_tables has been used)")

        trivial_transfers_id = get_trivial_transfer_vote_id(new_buffer_size, del_buffer_size, True, limit=vote_nbr_to_handle, seg_in_buffer=seg_in_buffer)

        for deleted_segment_id, new_segment_id in trivial_transfers_id:
            deleted_segment = SegmentsAll.objects.get(pk = int(deleted_segment_id))
            new_segment = Segments.objects.get(pk = int(new_segment_id))

            transfer_vote_from_to(deleted_segment, [new_segment])

    def prepare_table(self, new_buffer_size, del_buffer_size, seg_in_buffer):
        # Step 1 : Creation cyclab_segments_l93 (en option)
        with connection.cursor() as cursor:
            buffer_column = f', ST_Buffer(ST_Transform(s.geom, 2154), {new_buffer_size}) AS buff_l93 '

            cursor.execute('DROP TABLE IF EXISTS cyclab_segments_l93;')
            cursor.execute(f"""
CREATE TABLE cyclab_segments_l93 AS (
SELECT
    s.*, ST_Transform(s.geom, 2154) as geom_l93
    {buffer_column}
FROM  {settings.TABLE_NAME['segments']} as s);""")
        print("CREATE TABLE cyclab_segments_l93: DONE")

        # Step 2 : Creation index pour cyclab_segments_l93 (en option)
        with connection.cursor() as cursor:
            cursor.execute('CREATE INDEX ON "cyclab_segments_l93" USING GIST("geom_l93");')
            cursor.execute('CREATE INDEX ON "cyclab_segments_l93" ("id");')
            cursor.execute('CREATE INDEX ON "cyclab_segments_l93" USING GIST("buff_l93");')
        print("CREATE INDEX ON cyclab_segments_l93: DONE")


        # Step 3 :Creation deleted_cyclab_segments_l93 & index (en option)
        if del_buffer_size:
            buff_request_4_deleted = f", ST_Buffer(ST_Transform(s.geom, 2154), {del_buffer_size}) AS buff_l93 "
        else:
            buff_request_4_deleted = ""

        queries = [
            'DROP TABLE IF EXISTS deleted_cyclab_segments_l93;',
            f"""CREATE TABLE deleted_cyclab_segments_l93 AS (
                    SELECT
                        s.id AS id,
                        ST_Transform(s.geom, 2154) as geom_l93
                        {buff_request_4_deleted}
                        , count(*) AS vote_nbr
                    FROM {settings.TABLE_NAME['deleted_segments']} as s
                    JOIN {settings.TABLE_NAME['deleted_segment_votes']} as v ON v.segment_id = s.id
                    GROUP BY s.id, s.geom
                    HAVING COUNT(*) > 0);""",
            'CREATE INDEX ON "deleted_cyclab_segments_l93" USING  GIST("geom_l93");',
            'CREATE INDEX ON "deleted_cyclab_segments_l93" ("id");',
            'CREATE INDEX ON "deleted_cyclab_segments_l93" USING  GIST("buff_l93");',
        ]

        with connection.cursor() as cursor:
            for query in queries:
                cursor.execute(query)
        print("CREATE TABLE & INDEX ON deleted_cyclab_segments_l93: DONE")

        # Step 4 :Creation trivial_deleted_seg_for_transfer & index

        queries = [
            'DROP TABLE IF EXISTS del_segments_in_new_segment_buffer;',
        ]

        queries.append("""CREATE TABLE del_segments_in_new_segment_buffer AS (
SELECT del_s.id del_seg_id, s.id as new_seg_id__buff
FROM cyclab_segments_l93 as s
JOIN deleted_cyclab_segments_l93 as del_s ON ST_Covers(del_s.buff_l93, s.geom_l93));""")

        with connection.cursor() as cursor:
            for query in queries:
                cursor.execute(query)

        print("CREATE TABLE & INDEX ON trivial_deleted_seg_for_transfer: DONE")

# STEP 5

        queries = [
            'DROP TABLE IF EXISTS new_segments_in_del_segment_buffer;',
        ]

        queries.append("""CREATE TABLE new_segments_in_del_segment_buffer AS (
SELECT s.id new_seg_id, del_s.id as del_seg_id__buff
FROM cyclab_segments_l93 as s
JOIN deleted_cyclab_segments_l93 as del_s ON ST_Covers(s.buff_l93, del_s.geom_l93));""")

        with connection.cursor() as cursor:
            for query in queries:
                cursor.execute(query)

        # Step 6

        queries = [
            'DROP TABLE IF EXISTS del_segments_having_one_prop_to_trans;',
            f"""CREATE TABLE del_segments_having_one_prop_to_trans AS (
                SELECT dib.del_seg_id as del_seg_id, COUNT(*)
                FROM del_segments_in_new_segment_buffer as dib
                JOIN new_segments_in_del_segment_buffer as nib
                    ON dib.del_seg_id = nib.del_seg_id__buff
                    AND dib.new_seg_id__buff = nib.new_seg_id
                    GROUP BY dib.del_seg_id
                    HAVING COUNT(*) < {seg_in_buffer});"""
        ]

#         queries.append("""CREATE TABLE del_segments_having_one_prop_to_trans AS (
# SELECT dib.del_seg_id as del_seg_id, COUNT(*)
# FROM del_segments_in_new_segment_buffer as dib
# JOIN new_segments_in_del_segment_buffer as nib
#     ON dib.del_seg_id = nib.del_seg_id__buff
#     AND dib.new_seg_id__buff = nib.new_seg_id
#     GROUP BY dib.del_seg_id
#     HAVING COUNT(*) = 3);""")


        with connection.cursor() as cursor:
            for query in queries:
                cursor.execute(query)

        # Step 7

        queries = [
            'DROP TABLE IF EXISTS prop_transfer;',
        ]

        queries.append("""CREATE TABLE prop_transfer AS (
SELECT dib.del_seg_id as del_seg_id, nib.new_seg_id as new_seg_id
FROM del_segments_in_new_segment_buffer as dib
JOIN new_segments_in_del_segment_buffer as nib
ON dib.del_seg_id = nib.del_seg_id__buff
AND dib.new_seg_id__buff = nib.new_seg_id
AND dib.del_seg_id IN (SELECT del_seg_id FROM del_segments_having_one_prop_to_trans));""")

        with connection.cursor() as cursor:
            for query in queries:
                cursor.execute(query)

#         with connection.cursor() as cursor:
#             cursor.execute('SELECT COUNT(*) FROM public.prop_transfer;')

