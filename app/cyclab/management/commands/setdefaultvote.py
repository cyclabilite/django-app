from time import time, perf_counter
from sys import stdout
from django.core.management.base import BaseCommand
import psycopg2
from cyclab.vote_default.rules_to_apply import exe_rules
from cyclab.vote_default.updater import Updater

from django.conf import settings

class Command(BaseCommand):
    help = 'Sets a default vote value per segment based on type of segment etc...'

    def handle(self, *args, **options):
        # self.stdout.write("Test")
        global_start_t = time()
        global_start_c = perf_counter()

        print("## START DB CONNECTION")
        conn = psycopg2.connect(
            database=settings.DB_NAME,
            user=settings.DB_USER,
            password=settings.DB_PWD,
            host=settings.DB_HOST,
            port=settings.DB_PORT)
        cur = conn.cursor()
        print("# END db connection")
        stdout.flush()
        print("")

        updater = Updater(conn, cur, settings)
        updater.run(exe_rules)
        
        cur.close()
        conn.commit()
        conn.close()

        print("END")
        print("TOTAL TIME (t): " + str(time() - global_start_t))
        print("TOTAL TIME (c): " + str(perf_counter() - global_start_c))
