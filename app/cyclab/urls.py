from django.urls import path, include, register_converter
from django.contrib import admin
from .routers import router

from .views import admin as admin_views
from .views import osm as osm_views
from .views import static as static_views
from .views import user as user_views

from cyclab.converts import FloatUrlParameterConverter

register_converter(FloatUrlParameterConverter, 'float')

urlpatterns = [
    path('', static_views.index, name='index'),
    path('export/segments-notes.csv', static_views.export_segments_notes_csv, name='export_csv'),
    path('export/', static_views.export_page, name='export'),
    path('bboxcyclabsegments/', osm_views.bbox_segments,
        name='bbox_cyclab_segments'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('logout/', user_views.logout_view, name='logout'),
    path('login/', user_views.json_login, name='login'),
    path('user/<int:user_id>/',
        user_views.user_detail, name='user_detail'),
    path('register/', user_views.register_view, name='register'),
    path(
        'register/registration-success/',
        user_views.registration_success, name='registration_success'),
    path('register/email/validation/<slug:uidb64>/<slug:token>/',
        user_views.email_validation, name='email_validation'),
    path('api-auth/', include('rest_framework.urls')),
    path('help/index.html', static_views.help_page, name='help'),
    path('help/about.html', static_views.about, name='about'),
    path('help/data.html', static_views.data, name='data'),
    path('api/', include(router.urls)),
    path(
        'admin/vote-transfer/<int:deleted_segment_id>/',
        admin_views.vote_transfer, name='vote_transfer'),
    path(
        'admin/vote-transfer/<int:deleted_segment_id>/handle',
        admin_views.handle_vote_transfer, name='handle_vote_transfer'),
    path(
        'admin/set-in-frigo/<int:deleted_segment_id>/',
        admin_views.set_in_frigo, name='vote_in_frigo'),
    path('admin/step1_clean_trivial_transfer_tables/<float:new_buffer_size>/<float:del_buffer_size>/<int:seg_in_buffer>/',
        admin_views.step1_clean_trivial_transfer_tables, name='step1_clean_trivial_transfer_tables'
    ),
    path('admin/step2_generate_cyclab_segments_l93_table',
        admin_views.step2_generate_cyclab_segments_l93_table, name='step2_generate_cyclab_segments_l93_table'),
    path('admin/step3_generate_cyclab_segments_l93_index',
        admin_views.step3_generate_cyclab_segments_l93_index, name='step3_generate_cyclab_segments_l93_index'),
    path(
        'admin/prepare-trivial-transfer/<float:new_buffer_size>/<float:del_buffer_size>/<int:seg_in_buffer>/',
        admin_views.prepare_trivial_transfer, name='prepare_trivial_transfer'),
    path(
        'admin/check-trivial-transfer/',
        admin_views.check_trivial_transfer, name='check_trivial_transfer'),
    path(
        'admin/handle-trivial-transfer/<int:vote_nbr_to_handle>',
        admin_views.handle_trivial_transfer, name='handle_trivial_transfer'),
    path('admin/', admin.site.urls),
]
