from rest_framework import viewsets
from rest_framework.exceptions import MethodNotAllowed

from ..models.votes import VoteSegment, VoteReason
from ..serializers.votes import VoteSerializer, VoteReasonSerializer

class VoteViewSet(viewsets.ModelViewSet):
    queryset = VoteSegment.objects.all()
    serializer_class = VoteSerializer

    def list(self, request):
        """Deactivate list method (performance)"""
        raise MethodNotAllowed('GET', detail='Method "GET" not allowed without lookup')

class VoteReasonViewSet(viewsets.ModelViewSet):
    queryset = VoteReason.objects.all()
    serializer_class = VoteReasonSerializer
