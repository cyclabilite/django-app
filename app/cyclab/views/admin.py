import logging

from django.contrib.admin.views.decorators import staff_member_required
from django.core import serializers
from django.shortcuts import render, redirect
from django.db import connection
from django.http import JsonResponse, HttpResponse
from django.conf import settings

from cyclab.models import SegmentsDeleted, DeletedVoteSegment, SegmentsAll, Segments, VoteSegment

logger = logging.getLogger(__name__)

# ---------------------------------------------------------
# TRANSFER OF VOTES

# View to allow the transfer of votes from a deleted segment to an actual one.
@staff_member_required
def vote_transfer(request, deleted_segment_id):
    deleted_segment_id = int(deleted_segment_id)
    deleted_segment = SegmentsDeleted.objects.filter(pk=deleted_segment_id)
    deleted_segment_json = serializers.serialize('geojson', deleted_segment, fields=(
        'way', 'nodestart', 'nodeend', 'geom', 'vote_default', 'vote_number', 'vote_average'))

    votes = DeletedVoteSegment.objects.filter(segment=int(deleted_segment_id))

    deleted_segment_id = deleted_segment[0].id

    return render(request, 'admin/cyclab/vote_transfer.html', {
        'deleted_segment_id': deleted_segment_id,
        'deleted_segment_json': deleted_segment_json,
        'votes': votes
    })


def transfer_vote_from_to(from_segment: SegmentsAll, to_segments: list[Segments]) -> None:
    """
    Transfer all the vote from a segment to a list of segments. The transferred votes
    are deleted during the process.

    from_segment : the segment from start the transfer
    to_segments : the segments where the transfer is done
    """
    votes = DeletedVoteSegment.objects.filter(segment=int(from_segment.id))
    for vote in votes:
        for segment in to_segments:
            new_vote = VoteSegment.new_from(vote)
            new_vote.segment = Segments.objects.get(pk = segment.id)
            new_vote.save()
        vote.delete()

    from_segment.recompute()
    from_segment.save()



@staff_member_required
def handle_vote_transfer(request, deleted_segment_id):
    selected_segments_id_input = request.POST.get('selectedSegments')

    if selected_segments_id_input:
        selected_segments_id = map(int, selected_segments_id_input.split(','))
        deleted_segment = SegmentsAll.objects.get(pk = int(deleted_segment_id))
        selected_segments = Segments.objects.filter(id__in=selected_segments_id)

        transfer_vote_from_to(deleted_segment, selected_segments)

        next_segment_to_handle = SegmentsDeleted.objects.filter(vote_number__gt=0, infrigo=False).order_by('-id').first()

        if next_segment_to_handle:
            return redirect('vote_transfer', deleted_segment_id=next_segment_to_handle.id)
        else:
            return JsonResponse({'msg': 'all transfer has been handled'})
    else:
        return JsonResponse({'msg': 'no data to transfer has been recevied'}, status_code=404)


@staff_member_required
def set_in_frigo(request, deleted_segment_id):
    deleted_segment_id = int(deleted_segment_id)
    deleted_segment = SegmentsDeleted.objects.get(pk=deleted_segment_id)
    deleted_segment.infrigo = True
    deleted_segment.frigo_is_manuel = True
    deleted_segment.save()

    return redirect('/admin/cyclab/segmentsdeletedtohandle/')


def step1_clean_trivial_transfer_tables(request, new_buffer_size: float, del_buffer_size :float, seg_in_buffer :int):
    """
    Clean (remove) used  tables for trivial transfer + configure variables needs for the generation of the tables (buffer size)

    Drops the tables :
    - cyclab_segments_l93
    - deleted_cyclab_segments_l93

    (more explanations about the trivial transfer can be found at doc/trivial_transfer_protocol.md
    """
    with connection.cursor() as cursor:
        for table_name in ['cyclab_segments_l93', 'deleted_cyclab_segments_l93', 'prop_transfer']:
            cursor.execute(f"DROP TABLE IF EXISTS {table_name};")

    request.session['new_buffer_size'] = new_buffer_size
    request.session['del_buffer_size'] = del_buffer_size
    request.session['seg_in_buffer'] = seg_in_buffer
    request.session['re_init'] = True

    return JsonResponse({
        'msg': 'clean_trivial_transfer_tables : done',
        'params': {
            'new_buffer_size': request.session['new_buffer_size'],
            'del_buffer_size': request.session['del_buffer_size'],
            'seg_in_buffer': request.session['seg_in_buffer']
        }
    })

def step2_generate_cyclab_segments_l93_table(request):
    """
    Prepare table CyclabSegmentsL93 for trivial transfer

    Creates the table cyclab_segments_l93

    (more explanations about the trivial transfer can be found at doc/trivial_transfer_protocol.md #TODO
    """

    new_buffer_size = request.session['new_buffer_size']

    with connection.cursor() as cursor:
        buffer_column = f', ST_Buffer(ST_Transform(s.geom, 2154), {new_buffer_size}) AS buff_l93 '

        cursor.execute('DROP TABLE IF EXISTS cyclab_segments_l93;')
        cursor.execute(f"""
CREATE TABLE cyclab_segments_l93 AS (
SELECT
    s.*, ST_Transform(s.geom, 2154) as geom_l93
    {buffer_column}
FROM  {settings.TABLE_NAME['segments']} as s);""")


    return JsonResponse({
        'msg': 'step2_generate_cyclab_segments_l93_table : done',
        'params': {
            'new_buffer_size': request.session['new_buffer_size'],
            'del_buffer_size': request.session['del_buffer_size']
        }
    })

def step3_generate_cyclab_segments_l93_index(request):
    """
    Prepare index for the table CyclabSegmentsL93 for trivial transfer

    Creates an indexes on the table cyclab_segments_l93

    (more explanations about the trivial transfer can be found at doc/trivial_transfer_protocol.md
    """
    with connection.cursor() as cursor:
        cursor.execute('CREATE INDEX ON "cyclab_segments_l93" USING GIST("geom_l93");')
        cursor.execute('CREATE INDEX ON "cyclab_segments_l93" ("id");')


    return JsonResponse({
        'msg': 'step2_generate_cyclab_segments_l93_index : done',
        'params': {
            'new_buffer_size': request.session['new_buffer_size'],
            'del_buffer_size': request.session['del_buffer_size']
        }
    })

@staff_member_required
def prepare_trivial_transfer(request, new_buffer_size: float, del_buffer_size :float, seg_in_buffer :int):
    return render(request, 'admin/cyclab/prepare_trivial_transfer.html', {
        'new_buffer_size': new_buffer_size,
        'del_buffer_size': del_buffer_size,
        'seg_in_buffer': seg_in_buffer
    })


def generate_deleted_cyclab_segments_l93_table(del_buffer_size):
    """
    Generates the table deleted_cyclab_segments_l93
    (used for trivial transfer procedure)
    """
    if del_buffer_size:
        buff_request_4_deleted = f", ST_Buffer(ST_Transform(s.geom, 2154), {del_buffer_size}) AS buff_l93 "
    else:
        buff_request_4_deleted = ""

    queries = [
        'DROP TABLE IF EXISTS deleted_cyclab_segments_l93;',
        f"""CREATE TABLE deleted_cyclab_segments_l93 AS (
                SELECT
                    s.id AS id,
                    ST_Transform(s.geom, 2154) as geom_l93
                    {buff_request_4_deleted}
                    , count(*) AS vote_nbr
                FROM {settings.TABLE_NAME['deleted_segments']} as s
                JOIN {settings.TABLE_NAME['deleted_segment_votes']} as v ON v.segment_id = s.id
                GROUP BY s.id, s.geom
                HAVING COUNT(*) > 0);""",
        'CREATE INDEX ON "deleted_cyclab_segments_l93" USING  GIST("geom_l93");',
        'CREATE INDEX ON "deleted_cyclab_segments_l93" ("id");',
    ]

    if buff_request_4_deleted:
        queries.append('CREATE INDEX ON "deleted_cyclab_segments_l93" USING GIST("buff_l93");')

    with connection.cursor() as cursor:
        for querie in queries:
            cursor.execute(querie)

def generate_trivial_deleted_segment_table(seg_in_buffer):
    """
    Generate the  table trivial transfers
    (used for trivial transfer)
    """
    queries = [
        'DROP TABLE IF EXISTS del_segments_in_new_segment_buffer;',
    ]

    queries.append("""CREATE TABLE del_segments_in_new_segment_buffer AS (
SELECT del_s.id del_seg_id, s.id as new_seg_id__buff
FROM cyclab_segments_l93 as s
JOIN deleted_cyclab_segments_l93 as del_s ON ST_Covers(del_s.buff_l93, s.geom_l93));""")

    with connection.cursor() as cursor:
        for query in queries:
            cursor.execute(query)

# STEP 5

    queries = [
        'DROP TABLE IF EXISTS new_segments_in_del_segment_buffer;',
    ]

    queries.append("""CREATE TABLE new_segments_in_del_segment_buffer AS (
SELECT s.id new_seg_id, del_s.id as del_seg_id__buff
FROM cyclab_segments_l93 as s
JOIN deleted_cyclab_segments_l93 as del_s ON ST_Covers(s.buff_l93, del_s.geom_l93));""")

    with connection.cursor() as cursor:
        for query in queries:
            cursor.execute(query)

        # Step 6

    queries = [
        'DROP TABLE IF EXISTS del_segments_having_one_prop_to_trans;',
        f"""CREATE TABLE del_segments_having_one_prop_to_trans AS (
            SELECT dib.del_seg_id as del_seg_id, COUNT(*)
            FROM del_segments_in_new_segment_buffer as dib
            JOIN new_segments_in_del_segment_buffer as nib
                ON dib.del_seg_id = nib.del_seg_id__buff
                AND dib.new_seg_id__buff = nib.new_seg_id
                GROUP BY dib.del_seg_id
                HAVING COUNT(*) = {seg_in_buffer});"""
    ]

    with connection.cursor() as cursor:
        for query in queries:
            cursor.execute(query)

        # Step 7

    queries = [
        'DROP TABLE IF EXISTS prop_transfer;',
    ]

    queries.append("""CREATE TABLE prop_transfer AS (
SELECT dib.del_seg_id as del_seg_id, nib.new_seg_id as new_seg_id
FROM del_segments_in_new_segment_buffer as dib
JOIN new_segments_in_del_segment_buffer as nib
ON dib.del_seg_id = nib.del_seg_id__buff
AND dib.new_seg_id__buff = nib.new_seg_id
AND dib.del_seg_id IN (SELECT del_seg_id FROM del_segments_having_one_prop_to_trans));""")

    with connection.cursor() as cursor:
        for query in queries:
            cursor.execute(query)


def check_if_table_exists(table_names):
    """
    Check if tables in the list table_names exists in DB
    """
    sql_ok_table_names = map(lambda tn: f"'{tn}'", table_names) # add ' before and after the name

    sql = f"""SELECT COUNT(*)
        FROM information_schema.tables
        WHERE table_name = any(ARRAY[{','.join(sql_ok_table_names)}])"""

    with connection.cursor() as cursor:
        cursor.execute(sql)
        res = cursor.fetchone()[0]

        return int(res) == len(table_names)

    return False

def get_trivial_transfer_vote_id(new_buffer_size, del_buffer_size, re_init, limit, seg_in_buffer):
    if re_init and new_buffer_size and del_buffer_size and seg_in_buffer:

        # is del_buffer_size_can_become_null ?
        generate_deleted_cyclab_segments_l93_table(del_buffer_size)
        generate_trivial_deleted_segment_table(seg_in_buffer)

        must_have_tables = [ # tables that we need for the transfer
            'cyclab_segments_l93',
            'deleted_cyclab_segments_l93'
        ]

        if new_buffer_size and del_buffer_size:
            must_have_tables.append('prop_transfer')

        if check_if_table_exists(must_have_tables):
            if new_buffer_size and del_buffer_size:  # if buffer pour del et current
                query = f"""
SELECT trsf.del_seg_id as del_seg_id, new_seg.id as new_seg_id
FROM prop_transfer as trsf
JOIN deleted_cyclab_segments_l93 as del_seg
    ON trsf.del_seg_id = del_seg.id
JOIN cyclab_segments_l93 as new_seg
    ON ST_Covers(del_seg.buff_l93, new_seg.geom_l93)
    AND ST_Covers(new_seg.buff_l93, del_seg.geom_l93)
 LIMIT {limit};"""
            elif new_buffer_size: # if only buffer pour current
                query = f"""
SELECT del_seg.id as del_seg_id, new_seg.id as new_seg_id
FROM prop_transfer as del_seg
JOIN cyclab_segments_l93 as new_seg ON ST_Covers(new_seg.buff_l93, del_seg.geom_l93)
LIMIT {limit}; """
            else: # else only buffer pour del
                query = f"""
SELECT del_seg.id as del_seg_id, new_seg.id as new_seg_id
FROM prop_transfer as del_seg
JOIN cyclab_segments_l93 as new_seg ON ST_Covers(del_seg.buff_l93, new_seg.geom_l93)
LIMIT {limit}"""

            with connection.cursor() as cursor:
                cursor.execute(query)
                res = cursor.fetchall()
                return res


def get_trivial_transfer_vote_id_rq(request, limit:int):
    # param en session (?)
    new_buffer_size = request.session['new_buffer_size']
    del_buffer_size = request.session['del_buffer_size']
    seg_in_buffer = request.session['seg_in_buffer']
    re_init = request.session['re_init']

    get_trivial_transfer_vote_id(new_buffer_size, del_buffer_size, re_init, limit, seg_in_buffer)


@staff_member_required
def check_trivial_transfer(request):

    if 're_init' in request.session:
        logger.critical('OK')
    else:
        logger.critical('aie aie aei')
    re_init = True #  request.session['re-init']

    new_buffer_size = request.session['new_buffer_size']
    del_buffer_size = request.session['del_buffer_size']
    new_buffer_size = request.session['seg_in_buffer']

    if re_init and new_buffer_size and del_buffer_size and seg_in_buffer:
        limit = 50

        trivial_transfers_id = get_trivial_transfer_vote_id_rq(request, limit=limit)

        trivial_transfer_data = []
        for deleted_segment_id, new_segment_id in trivial_transfers_id:
            deleted_segment = SegmentsDeleted.objects.get(pk=deleted_segment_id)

            deleted_segment_json = serializers.serialize('geojson', [deleted_segment], fields=(
                'way', 'nodestart', 'nodeend', 'geom', 'vote_default', 'vote_number', 'vote_average'))

            new_segment= Segments.objects.get(pk=new_segment_id)
            new_segment_json = serializers.serialize('geojson', [new_segment], fields=(
                'way', 'nodestart', 'nodeend', 'geom', 'vote_default', 'vote_number', 'vote_average'))

            trivial_transfer_data.append({
                'del_seg': deleted_segment,
                'del_seg_json': deleted_segment_json,
                'new_seg': new_segment,
                'new_seg_json': new_segment_json
            })


        return render(request, 'admin/cyclab/check_trivial_transfer.html',
            {
                'trivial_transfer': trivial_transfer_data,
                'del_buffer_size': del_buffer_size,
                'new_buffer_size': new_buffer_size,
                'seg_in_buffer': seg_in_buffer
            }
        )

    else:
        html = "<html><body>ERROR : PAS BONNE INIT</body></html>"
        return HttpResponse(html, status_code=404)


@staff_member_required
def handle_trivial_transfer(request, vote_nbr_to_handle: int):
    new_buffer_size = request.session['new_buffer_size']
    del_buffer_size = request.session['del_buffer_size']

    if 're_init' in request.session:
        logger.critical('OK')
    else:
        logger.critical('aie aie aei')
    re_init = True #  request.session['re-init']


    if re_init and new_buffer_size and del_buffer_size:
        limit = vote_nbr_to_handle

        trivial_transfers_id = get_trivial_transfer_vote_id_rq(request, limit=limit)

        for deleted_segment_id, new_segment_id in trivial_transfers_id:
            deleted_segment = SegmentsAll.objects.get(pk = int(deleted_segment_id))
            new_segment = Segments.objects.get(pk = int(new_segment_id))

            transfer_vote_from_to(deleted_segment, [new_segment])

    return render(
        request,
        'admin/cyclab/handle_trivial_transfer.html',
        {'vote_nbr_to_handle': vote_nbr_to_handle})