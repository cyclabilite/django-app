# Views for static elements of the site :
# - static web pages (help, about, ....)
# - export file (CSV GENERATED)
import logging
from datetime import datetime

from django.shortcuts import render
from django.http import HttpResponse

from ..models import City

logger = logging.getLogger(__name__)

def index(request):
    """Index page for the app"""
    cities = City.objects.all()
    return render(request, 'cyclab/main.html', {'cities': cities})

def help_page(request):
    return render(request, 'cyclab/help.html')

def about(request):
    return render(request, 'cyclab/about.html')

def data(request):
    return render(request, 'cyclab/data.html')

def export_page(request):
    return render(request, 'cyclab/export.html')

def export_segments_notes_csv(request):
    file_content = open('/app/cyclab/media/export/segments-notes.csv', 'rb')
    response = HttpResponse(content=file_content)
    response['Content-Type'] = 'text/csv'
    response['Content-Disposition'] = \
        f'attachment; filename="export_{datetime.now().strftime("%y%m%d-%H%M%S")}.csv"'
    return response
