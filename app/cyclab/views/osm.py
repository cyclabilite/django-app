from django.core.serializers import serialize
from django.contrib.gis.geos import Polygon
from django.http import JsonResponse

from rest_framework import viewsets
from rest_framework.exceptions import MethodNotAllowed

import json

from ..models.osm import Segments, SegmentsDeleted, Ways
from ..serializers.osm import SegmentsSerializer, SegmentsDeletedSerializer, \
    WaysSerializer

def bbox_segments(request):
    """Gets all the segments within a given bounding-box"""
    bbox_str = request.GET
    bbox_arr = bbox_str["bbox"].split(",")
    # pprint(bbox_arr) TODO REMOVE ?

    bbox_arr = map(lambda x: float(x), bbox_arr)
    bb_polygon = Polygon.from_bbox(bbox_arr)

    bbox_segments = Segments.objects.filter(geom__intersects=bb_polygon).exclude(vote_default=0)

    serialized_bbox_segments = serialize('geojson', bbox_segments, fields=(
        'pk', 'way', 'nodestart', 'nodeend', 'geom', 'vote_default', 'vote_number', 'vote_average'))
    bbox_segments_json = json.loads(serialized_bbox_segments)
    return JsonResponse(bbox_segments_json, safe=False)

# GET SEGMENT DETAIL


def segment_detail(request, segment_id):
    segment_id = int(segment_id)
    segment_detail = Segments.objects.filter(pk=segment_id)

    serialized_segm_detail = serialize('geojson', segment_detail, fields=(
        'pk', 'way', 'nodestart', 'nodeend', 'geom', 'vote_default', 'vote_number', 'vote_average'))

    segment_detail_json = json.loads(serialized_segm_detail)
    return JsonResponse(segment_detail_json, safe=False)


# Views generated using DRF
class SegmentsViewSet(viewsets.ModelViewSet):
    queryset = Segments.objects.all()
    serializer_class = SegmentsSerializer

class SegmentsDeletedViewSet(viewsets.ModelViewSet):
    queryset = SegmentsDeleted.objects.all()
    serializer_class = SegmentsDeletedSerializer

class WaysViewSet(viewsets.ModelViewSet):
    queryset = Ways.objects.all()
    serializer_class = WaysSerializer

    def list(self, request):
        """Deactivate list method (performance)"""
        raise MethodNotAllowed('GET', detail='Method "GET" not allowed without lookup')

