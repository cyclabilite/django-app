# View associated to user account stuff (login, logout, ...)
import logging

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import render, redirect, reverse
from django.http import JsonResponse, HttpResponse
from django.contrib import messages
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator
from django.utils.translation import gettext as _

from rest_framework import viewsets
from rest_framework.exceptions import MethodNotAllowed

from ..forms import RegisterForm, UserDetailForm
from ..models.user import User, UserProfile
from ..serializers.user import UserSerializer, ProfileSerializer

logger = logging.getLogger(__name__)

def json_login(request):
    if request.method == "POST":
        email = request.POST.get('username')
        password = request.POST.get('password')

        print(f"Email: {email}")
        print(f"Password: {password}")


        user = authenticate(request, email=email, password=password)

        if not user: # user is None :
            data = {
                'status_code': 401,
                'message': _('Username or password are incorrect, try again.')
            }
        elif not user.is_active:
            data = {
                'status_code': 401,
                'message': _('This account is no longer active')
            }
        elif not user.is_email_verified:
            data = {
                'status_code': 401,
                'message': _('You should activate your account using the link sent by email')
            }
        else:
            login(request, user)
            data = {
                'user': user.id,
                'status_code': 200,
                'message': _('You have successfully logged in')
            }
    else:
        form = AuthenticationForm()
        return render(request, 'registration/login.html', {'form': form})

    return JsonResponse(data)


def email_validation(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except User.DoesNotExist:
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        # default_token_generator are valid for 3 days see PASSWORD_RESET_TIMEOUT
        user.is_email_verified = True
        user.save()
        login(request, user)

        messages.success(request, _('Your account has been confirmed and you are now logged in'))

        return redirect(registration_success)
    else:
        messages.error(request, _('Activation link is invalid! Please contact us at info@droitauvelo.org'))
        return redirect(registration_success)

def logout_view(request):
    logout(request)
    return redirect('index')

def register_view(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _('You have succesfully registered. Please confirm your account through the email that has been sent'))

            return redirect(registration_success)
        else:
            messages.error(request, "Error")
    else:
        form = RegisterForm()
    return render(request, 'registration/register.html', {'form': form})

def registration_success(request):

    return render(request, 'registration/registration-success.html')

def user_detail(request, user_id):

    if request.method == 'POST':
        form = UserDetailForm(request.POST, instance = request.user)
        if form.is_valid():
            form.save()
            messages.success(request, _('Your details have been succesfully modified'))
    else:
        form = UserDetailForm(instance = request.user)
    return render(request, 'cyclab/user_detail.html', {
            'user': request.user,
            'form': form
    })
class UserViewSet(viewsets.ModelViewSet):
    # TODO VERIFIER QUE ça pose pas problème si accès aux données users vi API (fuite de données)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def list(self, request):
        """Deactivate list method (security)"""
        raise MethodNotAllowed('GET', detail='Method "GET" not allowed without lookup')


class ProfileViewSet(viewsets.ModelViewSet):
    # TODO VERIFIER QUE ça pose pas problème si accès aux données users vi API (fuite de données)
    queryset = UserProfile.objects.all()
    serializer_class = ProfileSerializer

    def list(self, request):
        """Deactivate list method (security)"""
        raise MethodNotAllowed(
            'GET', detail='Method "GET" not allowed without lookup')