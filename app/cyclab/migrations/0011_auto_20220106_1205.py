from django.db import migrations
from datetime import datetime
from django.utils.timezone import make_aware

import random
import string

import logging
logger = logging.getLogger(__name__)

def clean_db(apps, schema_editor):
    """Remove all the element from the django user and profile tables"""
    with schema_editor.connection.cursor() as cursor:
        cursor.execute("DELETE FROM  cyclab_user") # remise à zéro de la db
        cursor.execute("DELETE FROM  cyclab_userprofile") # remise à zéro de la db


def migration_of_users(apps, schema_editor):
    """ migrate all data from cyclab_fos_user (the symfony user table) to the django user table (+ good configuration for the next-user seq)"""
    User = apps.get_model('cyclab', 'User')

    with schema_editor.connection.cursor() as cursor:
        cursor.execute("SELECT id, email, username, last_login, gender, birthyear, creationdate, isexpert FROM cyclab_fos_users WHERE enabled=True")
        rows = cursor.fetchall()
        user_id_max = 0

        for row in rows:
            user_id, user_email, user_username, user_ll, user_gender, user_birthyear, user_cd, user_isexpert = row

            user = User.objects.model(
                id=user_id,
                email=user_email,
                first_name="",
                last_name="",
            )

            user.password =  ''.join(random.choice(string.ascii_letters) for i in range(10))
            user.username = user_username
            user.birth_date =  make_aware(datetime(year=user_birthyear, month=1, day=1, second=59))
            user.is_expert =  user_isexpert
            user.date_joined = make_aware(user_cd)

            if user_gender == 'male':
                user.gender = 'h'
            elif user_gender == 'female':
                user.gender = 'f'

            user_id_max = max(user_id_max, user_id)

            user.save()
        cursor.execute(f"SELECT setval('cyclab_user_id_seq', {user_id_max + 1});")
        
def migration_of_profiles(apps, schema_editor):
    """ migrate all data from cyclab_profile (the symfony user table) to the django profile table  (+ good configuration for the next-profile seq)"""

    UserProfile = apps.get_model('cyclab', 'UserProfile')
    User = apps.get_model('cyclab', 'User')

    with schema_editor.connection.cursor() as cursor:

        cursor.execute("SELECT id, label, user_id, daily_ride, rapidity_vs_security, active  FROM cyclab_profile")
        rows = cursor.fetchall()
        profile_id_max = 0

        for row in rows:
            p_id, p_label, p_user_id, p_daily_ride, p_rap_sec, p_active = row
            profile_id_max = max(profile_id_max, p_id)

            user = User.objects.get(pk=p_user_id)

            profile = UserProfile.objects.model(
                id = p_id,
                user=user,
                rider_type=(0 if p_daily_ride else 1),
                rapidity = p_rap_sec,
                is_active = p_active)

            profile.save()

        cursor.execute(f"SELECT setval('cyclab_userprofile_id_seq', {profile_id_max + 1});")
        
class Migration(migrations.Migration):

    dependencies = [
        ('cyclab', '0010_auto_20220106_1205'),
    ]

    operations = [
        migrations.RunSQL(
            "DROP TABLE IF EXISTS cyclab_users", reverse_sql=migrations.RunSQL.noop),
        migrations.RunPython(clean_db, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(migration_of_users, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(migration_of_profiles, reverse_code=migrations.RunPython.noop),
    ]