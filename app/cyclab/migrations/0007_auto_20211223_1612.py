# Generated by Django 3.1.7 on 2021-12-23 15:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cyclab', '0006_auto_20211223_1305'),
    ]

    operations = [
        migrations.RunSQL("""CREATE OR REPLACE VIEW ways_all AS (
            SELECT id, name, tstamp, highway, linestring, changeset_id, version, tags, user_id,  vote_default, vote_number, vote_average, nodes FROM ways 
        ) UNION ALL  (
            SELECT id, name, tstamp, highway, linestring, changeset_id, version, tags, user_id,  vote_default, vote_number, vote_average, nodes FROM deleted_ways 
        );
        """)
    ]
