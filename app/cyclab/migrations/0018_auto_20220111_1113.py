from django.db import migrations
from django.utils.timezone import make_aware

import logging
logger = logging.getLogger(__name__)

def clean_db(apps, schema_editor):
    """Remove all the votes from  cyclab_deletedvotesegment"""
    with schema_editor.connection.cursor() as cursor:
        cursor.execute("DELETE FROM cyclab_deletedvotesegment")

def migration_of_segment_votes(apps, schema_editor):
    """ migrate all the deleted votes for the segment """
    DeletedVoteSegment = apps.get_model('cyclab', 'DeletedVoteSegment')
    User = apps.get_model('cyclab', 'User')
    UserProfile = apps.get_model('cyclab', 'UserProfile')
    SegmentsAll  = apps.get_model('cyclab', 'SegmentsAll')

    with schema_editor.connection.cursor() as cursor:
        cursor.execute("SELECT id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, is_expert_vote, segment_id, direction FROM deleted_cyclab_segment_vote")
        rows = cursor.fetchall()
        cpt = 0

        for row in rows:
            vote_id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, is_expert_vote, segment_id, direction  = row

            user = User.objects.get(pk=user_id)
            profile = UserProfile.objects.get(pk=profile_id)
            segment = SegmentsAll.objects.get(pk=segment_id)

            vote = DeletedVoteSegment.objects.model(
                id=vote_id,
                vote_value=vote_value,
                profile = profile,
                user = user,
                segment = segment)

            if direction == 'both':
                vote.direction = 'BOTH'
            elif direction == 'forward':
                vote.direction = 'AB'
            else:
                vote.direction = 'BA'

            vote.vote_date_time=make_aware(datetime_vote)
            vote.vote_creation_time = make_aware(datetime_creation)
            vote.save()

            if cpt % 2500 == 0:
                logger.critical(f" ({cpt})")
                DeletedVoteSegment.objects.update()
            cpt = cpt + 1
        
class Migration(migrations.Migration):

    dependencies = [
        ('cyclab', '0017_auto_20220111_1002'),
    ]

    operations = [
        migrations.RunPython(clean_db, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(migration_of_segment_votes, reverse_code=migrations.RunPython.noop),
    ]
