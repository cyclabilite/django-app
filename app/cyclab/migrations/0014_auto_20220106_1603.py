from django.db import migrations
from django.utils.timezone import make_aware

import logging
logger = logging.getLogger(__name__)

def clean_db(apps, schema_editor):
    """Remove all the existing votes"""
    with schema_editor.connection.cursor() as cursor:
        cursor.execute("DELETE FROM  cyclab_votesegment")

def migration_of_segment_votes(apps, schema_editor):
    """ migrate all the votes for the segment """
    VoteSegment = apps.get_model('cyclab', 'VoteSegment')
    User = apps.get_model('cyclab', 'User')
    UserProfile = apps.get_model('cyclab', 'UserProfile')
    Segments  = apps.get_model('cyclab', 'Segments')

    cpt = 0

    with schema_editor.connection.cursor() as cursor:
        cursor.execute("SELECT id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, is_expert_vote, segment_id, direction FROM cyclab_segment_vote")
        rows = cursor.fetchall()
        vote_id_max = 0

        for row in rows:
            # PBL DATETIME (?)
            vote_id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, is_expert_vote, segment_id, direction  = row
            vote_id_max = max(vote_id_max, vote_id)

            user = User.objects.get(pk=user_id)
            profile = UserProfile.objects.get(pk=profile_id)
            segment = Segments.objects.get(pk=segment_id)

            vote = VoteSegment.objects.model(
                id=vote_id,
                vote_value=vote_value,
                profile = profile,
                user = user,
                segment = segment)

            if direction == 'both':
                vote.direction = 'BOTH'
            elif direction == 'forward':
                vote.direction = 'AB'
            else:
                vote.direction = 'BA'

            vote.vote_date_time=make_aware(datetime_vote)
            vote.vote_creation_time = make_aware(datetime_creation)
            vote.save()

            if cpt % 2500 == 0:
                logger.critical(f" ({cpt})")
                VoteSegment.objects.update()
            cpt = cpt + 1

           
        cursor.execute(f"SELECT setval('cyclab_vote_segment_id_seq', {vote_id_max + 1});")

        
class Migration(migrations.Migration):

    dependencies = [
        ('cyclab', '0013_auto_20220106_1603'),
    ]

    operations = [
        migrations.RunPython(clean_db, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(migration_of_segment_votes, reverse_code=migrations.RunPython.noop),
    ]
