from django.contrib.gis.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from .votes import VoteSegment

import logging
logger = logging.getLogger(__name__)

class AbstractNodes (models.Model):
    id = models.BigIntegerField(primary_key=True)
    version = models.IntegerField()
    user_id = models.IntegerField()
    tstamp = models.DateTimeField()
    changeset_id = models.BigIntegerField()
    # This field type is a guess.
    tags = models.TextField(blank=True, null=True)
    geom = models.PointField(blank=True, null=True)
    ways_nbr = models.IntegerField(blank=True, null=True)

    class Meta:
        abstract = True

class Nodes(AbstractNodes):

    class Meta:
        managed = False
        db_table = 'nodes'

class DeletedNodes(AbstractNodes):

    class Meta:
        managed = False
        db_table = 'deleted_nodes'

class AbstractWays (models.Model):
    id = models.BigIntegerField(primary_key=True)
    version = models.IntegerField()
    user_id = models.IntegerField()
    tstamp = models.DateTimeField()
    changeset_id = models.BigIntegerField()
    # This field type is a guess.
    tags = models.TextField(blank=True, null=True)
    # This field type is a guess.
    nodes = models.TextField(blank=True, null=True) # todo link avec nodes ?
    linestring = models.GeometryField(blank=True, null=True)

    class Meta:
        abstract = True

class Ways(AbstractWays):

    class Meta:
        managed = False
        db_table = 'ways'

class WaysAll(AbstractWays):

    class Meta:
        managed = False
        db_table = 'ways_all'

class DeletedWays(AbstractNodes):

    class Meta:
        managed = False
        db_table = 'deleted_ways'

class AbstractSegments(models.Model):
    id = models.BigAutoField(primary_key=True)
    nodestart = models.ForeignKey(
        Nodes, models.DO_NOTHING, blank=True, null=True, related_name="%(class)s_start_related")
    nodeend = models.ForeignKey(
        Nodes, models.DO_NOTHING, blank=True, null=True, related_name="%(class)s_end_related")
    geom = models.LineStringField(blank=True, null=True)
    vote_default = models.IntegerField(blank=True, null=True)
    vote_number = models.IntegerField(default=0)
    vote_average = models.FloatField(blank=True, null=True, default=None)
    vote_average_both = models.FloatField(blank=True, null=True)
    vote_average_ab = models.FloatField(blank=True, null=True)
    vote_average_ba = models.FloatField(blank=True, null=True)

    def get_votes_by_profiles(self):
        """creation of a dict with profile_id as key and the list of votes as value"""
        votes_by_profile = {}

        for v in self.votes.all().order_by('-vote_creation_time'):
            if v.profile.id in votes_by_profile:
                votes_by_profile[v.profile.id].append(v)
            else:
                votes_by_profile[v.profile.id] = [v]

        return votes_by_profile

    def get_votes_to_compute(self):
        """
            List of couples of votes to consider for the computation of the average :
            - each couple of the list is associated to a profile
            - the first element of the couple is always a vote (the lastest)
            - the second element of the couple is
                - either a vote : if there exists a vote before the latestest whith another direction (to compute AB and BA)
                - either null
        """
        votes_to_compute = []
        votes_by_profile = self.get_votes_by_profiles()

        for profile_id in votes_by_profile:
            votes = votes_by_profile[profile_id]

            v_latest = votes[0]
            v_latest_next = None # 2 eme vote qui a de l'important si v_latest est AB, alors v_latest2 doit représenter BA

            if v_latest.direction != 'BOTH':
                for v in votes[1:]:
                    if ( v.direction == 'BOTH'
                        or (v_latest.direction == 'AB' and v.direction == 'BA')
                        or (v_latest.direction == 'BA' and v.direction == 'AB')
                        ):
                            v_latest_next = v
                            break

            votes_to_compute.append((v_latest, v_latest_next))

        return votes_to_compute

    def update_votes_lastest(self, save=True):
        latests_votes_by_profile = self.get_votes_to_compute()
        latests_votes_list =  [v for couple in latests_votes_by_profile for v in couple if v is not None]
        changed_votes = []

        for v in self.votes.all():
            if v in latests_votes_list:
                if v.latest is False:
                    v.latest = True
                    changed_votes.append(v)
                    if save:
                        v.save()
            else:
                if v.latest is True:
                    v.latest = False
                    changed_votes.append(v)
                    if save:
                        v.save()
        return changed_votes

    def compute_avg(self, sum1, cpt1, sum2, cpt2):
        """compute ((sum1 / cpt1) + (sum2 / cpt2)) / 2 (handle when cpt1 or cpt2 = 0"""
        if cpt1 == 0:
            if cpt2 == 0:
                return None
            else:
                return round((sum2 / cpt2), 1)
        else:
            if cpt2 == 0:
                return round((sum1 / cpt1), 1)
            else:
                return round((((sum1 / cpt1) + (sum2 / cpt2)) / 2), 1)

    def recompute(self):
        """Recompute the votes (ALL = main incide & BOTH, AB, BA : given a direction"""
        votes_to_compute = self.get_votes_to_compute()

        sum_normal = {'BOTH': 0, 'AB': 0, 'BA': 0, 'ALL': 0}
        sum_expert = {'BOTH': 0, 'AB': 0, 'BA': 0, 'ALL': 0}
        cpt_normal = {'BOTH': 0, 'AB': 0, 'BA': 0, 'ALL': 0}
        cpt_expert = {'BOTH': 0, 'AB': 0, 'BA': 0, 'ALL': 0}

        for (v_latest, v_latest_next) in votes_to_compute:
            # choice of good avg, cpt var
            summ = sum_normal
            cpt = cpt_normal

            if v_latest.is_expert():
                summ =  sum_expert
                cpt = cpt_expert

            vote_to_add = v_latest.vote_value # if BOOTH (or single)
            if v_latest_next: # otherwise a moyenne of AB + BA
                vote_to_add = (v_latest.vote_value + v_latest_next.vote_value) / 2
            cpt['ALL'] += 1
            summ['ALL'] += vote_to_add

            cpt[v_latest.direction] += 1
            summ[v_latest.direction] += v_latest.vote_value

            if v_latest_next:
                cpt[v_latest_next.direction] += 1
                summ[v_latest_next.direction] += v_latest_next.vote_value


        self.vote_number = cpt_normal['ALL'] + cpt_expert['ALL']
        self.vote_average = self.compute_avg(sum_normal['ALL'], cpt_normal['ALL'], sum_expert['ALL'], cpt_expert['ALL'])
        self.vote_average_both = self.compute_avg(sum_normal['BOTH'], cpt_normal['BOTH'], sum_expert['BOTH'], cpt_expert['BOTH'])
        self.vote_average_ab   = self.compute_avg(sum_normal['AB'], cpt_normal['AB'], sum_expert['AB'], cpt_expert['AB'])
        self.vote_average_ba   = self.compute_avg(sum_normal['BA'], cpt_normal['BA'], sum_expert['BA'], cpt_expert['BA'])

    class Meta:
        abstract = True

class Segments(AbstractSegments):
    way = models.ForeignKey(WaysAll, models.DO_NOTHING,
                            related_name='segments', blank=True, null=True)

    @receiver(post_save, sender=VoteSegment)
    def my_handler(sender, instance, **kwargs):
        s = Segments.objects.get(id=instance.segment.id)
        s.recompute()
        s.update_votes_lastest()
        s.save()

    class Meta:
        managed = False
        db_table = 'cyclab_segments'

class SegmentsDeleted(AbstractSegments):
    infrigo = models.BooleanField(null=True)
    frigo_datetime = models.DateField(null=True)
    frigo_is_manuel = models.BooleanField(null=True)

    way = models.ForeignKey(WaysAll, models.DO_NOTHING,
                            related_name='segmentsDeleted', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'deleted_cyclab_segments'

class SegmentsDeletedToHandle(SegmentsDeleted):
    """ To have a special page for deleted segments to
    handle manually in the AdminSite"""

    class Meta:
        proxy = True


class SegmentsAll(AbstractSegments):

    class Meta:
        managed = False
        db_table = 'cyclab_segments_all'


    def save(self, *args, **kwargs):
        # Save do not work as it is a view having an union
        try:
            segment = Segments.objects.get(pk=self.id)
        except Segments.DoesNotExist:
            try:
                segment = SegmentsDeleted.objects.get(pk=self.id)
            except SegmentsDeleted.DoesNotExist:
                segment = None

        if segment:
            logger.warning('Limited update for SegmentAll objet')
            segment.vote_number = self.vote_number
            segment.vote_average = self.vote_average
            segment.vote_average_both = self.vote_average_both
            segment.vote_average_ab = self.vote_average_ab
            segment.vote_average_ba = self.vote_average_ba
            segment.save()
        else:
            raise Exception('Not possible to save this object')
