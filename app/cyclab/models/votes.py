from django.contrib.gis.db import models
from .user import UserProfile, User
from datetime import datetime

class VoteReason(models.Model):
    VALUE_CHOICES = (
        ("Positive", "Positive"),
        ("Negative", "Negative")
    )
    description = models.CharField(max_length=250, null=False)
    reason_value = models.CharField(max_length=10, choices=VALUE_CHOICES, default="Positive")
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.description

class VoteAbstract(models.Model):
    # Value choices
    VOTE_CHOICES = (
        (1, "Very poor quality"),
        (2, "Poor quality"),
        (3, "OK quality"),
        (4, "Good quality"),
        (5, "Excellent quality")
    )

    DIRECTION_CHOICES = (
        ("AB", "Forward"),
        ("BA", "Backward"),
        ("BOTH", "Both")
    )

    vote_value = models.IntegerField(choices=VOTE_CHOICES)
    # vote_date_time = models.DateTimeField(
    #     null=True, blank=True, default=datetime.now) # date that can be given by the user
    vote_creation_time = models.DateTimeField(
        null=True, blank=True, default=datetime.now) # date when the vote has be encoded

    user = models.ForeignKey(User, default=None, null=True, on_delete=models.SET_NULL)
    profile = models.ForeignKey(
        UserProfile, default=None, null=True, on_delete=models.CASCADE)

    direction = models.CharField(
        max_length=4, choices=DIRECTION_CHOICES)
    vote_reason = models.ForeignKey(VoteReason, default=None, null=True, on_delete=models.CASCADE)

    latest = models.BooleanField(default=False)

    def reason_text(self):
        if self.vote_reason:
            return self.vote_reason.description
        return '-'

    def save(self, *args, **kwargs):
        if not self.profile:
            self.profile = self.user.profiles.first()
        super().save(*args, **kwargs)

    def is_expert(self):
        return self.user.is_expert

    def username(self):
        return self.user.username

    def transfer_data_to(self, new_vote):
        new_vote.vote_value = self.vote_value
        new_vote.vote_creation_time = self.vote_creation_time
        new_vote.user = self.user
        new_vote.profile = self.profile
        new_vote.direction = self.direction
        new_vote.vote_reason = self.vote_reason

    class Meta:
        abstract = True

class VoteSegment(VoteAbstract):
    segment = models.ForeignKey(
        "Segments", related_name='votes', default=None, on_delete=models.CASCADE)

    user = models.ForeignKey(User, related_name='votes', default=None, null=True, on_delete=models.SET_NULL)
    profile = models.ForeignKey(
        UserProfile, related_name='votes', default=None, null=True, on_delete=models.CASCADE)

    @classmethod
    def new_from(cls, old_vote):
        new_vote = cls()
        old_vote.transfer_data_to(new_vote)
        return new_vote

class DeletedVoteSegment(VoteAbstract):
    segment = models.ForeignKey(
        "SegmentsAll", related_name='votes', default=None, on_delete=models.CASCADE, db_constraint=False)

    user = models.ForeignKey(User, related_name='deleted_votes', default=None, null=True, on_delete=models.SET_NULL)
    profile = models.ForeignKey(
        UserProfile, related_name='deleted_votes', default=None, null=True, on_delete=models.CASCADE)
