from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.template.loader import get_template
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.conf import settings

import logging
logger = logging.getLogger(__name__)

class UserManager(BaseUserManager):
    def create_user(
            self, email, first_name, last_name, password=None,
            commit=True):
        """
        Creates and saves a User with the given email, first name, last name
        and password.
        """
        if not email:
            raise ValueError(_('Users must have an email address'))
        if not first_name:
            raise ValueError(_('Users must have a first name'))
        if not last_name:
            raise ValueError(_('Users must have a last name'))

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        if commit:
            user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password):
        """
        Creates and saves a superuser with the given email, first name,
        last name and password.
        """
        user = self.create_user(
            email,
            password=password,
            first_name=first_name,
            last_name=last_name,
            commit=False,
        )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    GENDER_CHOICES = (
        ('h', _('Male')),
        ('f', _('Female')),
    )

    email = models.EmailField(
        verbose_name=_('email address'), max_length=255, unique=True
    )
    # password field supplied by AbstractBaseUser
    # last_login field supplied by AbstractBaseUser
    username = models.CharField(max_length=200, blank=False, unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    birth_year = models.IntegerField(null=True, blank=True)
    is_expert = models.BooleanField(default=False)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, null=True)
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'
        ),
    )
    # is_superuser field provided by PermissionsMixin
    # groups field provided by PermissionsMixin
    # user_permissions field provided by PermissionsMixin
    is_email_verified = models.BooleanField(default=False)

    date_joined = models.DateTimeField(
        _('date joined'), default=timezone.now
    )

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = f'{self.first_name} {self.last_name}'
        return full_name.strip()

    def __str__(self):
        return f'{self.get_full_name()} <{self.email}>'

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

@receiver(post_save, sender=User)
def automatic_profile_creation(sender, instance, created, **kwargs):
    if created:
        profile = UserProfile(user=instance, rider_type=3, rapidity=4)
        profile.save()

@receiver(post_save, sender=User)
def send_email_check(sender, instance, created, **kwargs):
    user = instance
    logger.critical("before if condition")
    if created:
        logger.critical("send the email")

        context = {
            'user': user,
            'token':str(default_token_generator.make_token(user)),
            'uidb64': str(urlsafe_base64_encode(force_bytes(user.pk))),
            'url_base': settings.SITE_URL,
        }

        email_template = get_template('registration/email_activation.txt')
        email_content = email_template.render(context)

        send_mail(
            _('Please activate your account'),
            email_content,
            None,
            [user.email],
            fail_silently=False,
        )

class UserProfile(models.Model):
    RAPIDITY_CHOICES = (
        (0, _('Louveteau')),
        (1, _('Tortue')),
        (2, _('Champion'))
    )

    TYPE_CHOICES = (
        (0, _('Daily rider')),
        (1, _('Leasure rider'))
    )

    user = models.ForeignKey(
        User, related_name='profiles', on_delete=models.CASCADE)
    rider_type = models.SmallIntegerField(choices=TYPE_CHOICES, null=True)
    rapidity = models.SmallIntegerField(choices=RAPIDITY_CHOICES, null=True)
    is_active = models.BooleanField(default=True)
