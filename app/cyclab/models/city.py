from django.db import models
from django.contrib.gis.db import models


# --------------------------------------------------------
# CITIES


class City(models.Model):
    city_name = models.CharField(max_length=255)
    point = models.PointField()
    zoom_level = models.IntegerField(
        default=10)
    is_default = models.BooleanField(
        default=False)

    def change_default(self):
        self.is_default = False

    @property
    def lat(self):
        return self.point.y

    @property
    def lon(self):
        return self.point.x

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'

    def __str__(self):
        return self.city_name
