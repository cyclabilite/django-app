import time
import sys

class Updater():
    help = 'Sets a default vote value per segment based on type of segment etc...'

    def __init__(self, conn, cur, settings):
        self.conn = conn
        self.cur = cur
        self.settings = settings
        self.start_t = None
        self.start_c = None

    # def compute_hidden_intersections(self):
    #     """
    #     Compute the hidden intersections :
    #         - intersection only between hidden segments (with a default_vote of 0)
    #     """
    #     print("## START computing the hidden intersections")
    #     self.start_t = time.time()
    #     self.start_c = time.perf_counter()
    #     self.cur.execute("\
    #         CREATE TEMP TABLE tmp_node_id_with_default_vote ON COMMIT DROP AS \
    #         SELECT i.id  AS id, GREATEST(MAX(ss.vote_default), MAX(es.vote_default)) as max_default_vote \
    #         FROM cyclab_intersections AS i \
    #         LEFT JOIN cyclab_segments AS ss ON ss.nodestart_id = i.id \
    #         LEFT JOIN cyclab_segments AS es ON es.nodeend_id = i.id \
    #         GROUP BY i.id;")

    #     self.cur.execute("\
    #         UPDATE cyclab_intersections as i \
    #         SET vote_default = tmp.max_default_vote \
    #         FROM tmp_node_id_with_default_vote AS tmp \
    #         WHERE tmp.id = i.id \
    #         AND tmp.max_default_vote = 0;")

    #     print("# END computing the hidden intersections")
    #     print("# TIME (t): " + str(time.time() - self.start_t))
    #     print("# TIME (c): " + str(time.perf_counter() - self.start_c))
    #     sys.stdout.flush()
    #     print("")


    def rel_vote(self, rel_tag_value, vote, condition_link="or"):
        """
        idem que and_tag_value_vote mais pour les ways et semgents qui
        appartiennent a une relation selectionnee par rel_tag_value
        et condition_link
        """
        print("## START REL " + str(rel_tag_value) + " -> " + str(vote))
        self.start_t = time.time()
        self.start_c = time.perf_counter()

        condition = "((r.tags->'" + rel_tag_value[0][0] + "') = '" \
            + rel_tag_value[0][1] + "'"

        for (tag, value) in rel_tag_value[1:]:
            condition += " " + condition_link + " (r.tags->'" + tag + "') = '" \
                + value + "'"
        condition += ")"

        # need help about relation_members (ici relatio osm )
        self.cur.execute(f"UPDATE {self.settings.TABLE_NAME['segments']} AS s SET vote_default = " + str(vote)
                    + " FROM relation_members AS rw, relations AS r "
                    + " WHERE s.way_id = rw.member_id AND r.id = rw.relation_id "
                    + "AND " + condition)
                    # relation_members ?

        self.cur.execute(f"UPDATE {self.settings.TABLE_NAME['ways']} as w SET vote_default = " + str(vote)
                    + " FROM relation_members AS rw, relations "
                    + " AS r WHERE w.id = rw.member_id AND r.id = rw.relation_id "
                    + " AND " + condition)

        print("# END REL " + str(rel_tag_value) + " -> " + str(vote))
        print("# TIME (t): " + str(time.time() - self.start_t))
        print("# TIME (c): " + str(time.perf_counter() - self.start_c))
        sys.stdout.flush()
        print("")


    def get_condition_for_tuple(self, t):
        """
            Pour un couple (key, value) stocké en db hstore, retourne
            la conditions (w.tags->'key'='value')

            (see Tests)
        """
        return f"(w.tags->'{t[0]}'='{t[1]}')"


    def compute_condition(self, tag_value_array, and_level=True):
        """
            Transforme un tableau de clés/valeurs (se trouvant stocké dans en db dans
            du hstore) en un condition pour postgresql. Le tableau est interprété
            de manière récursive en séparant les elements par des 'and' ou des
            'or' en alternant à chaque niveau (voir tests qui sont plus parlants)

            (see Tests)
        """
        tag_value_array_0 = tag_value_array[0]

        if type(tag_value_array_0) == list:
            condition = self.compute_condition(tag_value_array_0, not and_level)
        else:
            condition = self.get_condition_for_tuple(tag_value_array_0)

        for tag_value_array_i in tag_value_array[1:]:
            if type(tag_value_array_i) == list:
                condition_i = self.compute_condition(tag_value_array_i, not and_level)
            else:
                condition_i = self.get_condition_for_tuple(tag_value_array_i)

            condition_link = 'and'
            if not and_level:
                condition_link = 'or'
            condition += f" {condition_link} {condition_i}"
        return f"({condition})"


    def and_tag_value_vote(self, tag_value_array, vote, and_for_first_level=True):
        """
        met $vote dans vote_default pour les tables cyclab_segments et ways tels
        que
            - ways a les tags/value dans tag_value_array (si condition_link="and"
                c'est un and sinon un or)
            - segments appartient a un ways ci dessus
        """
        print("# START WAY " + str(tag_value_array) + " -> " + str(vote))
        self.start_t = time.time()
        self.start_c = time.perf_counter()

        condition = self.compute_condition(tag_value_array, and_for_first_level)
        print("# CONDITION :")
        print(f"{condition}")

        self.cur.execute(f"UPDATE {self.settings.TABLE_NAME['segments']} AS s SET vote_default = " + str(vote)
                    + f" FROM {self.settings.TABLE_NAME['ways']} as w  WHERE s.way_id = w.id  and " + condition)

        self.cur.execute(f"UPDATE {self.settings.TABLE_NAME['ways']}  as w SET vote_default = " + str(vote)
                    + " WHERE " + condition)
        print("# END WAY " + str(tag_value_array) + " -> " + str(vote))
        print(f"# TIME (t): {time.time() - self.start_t}")
        print(f"# TIME (c): {time.perf_counter() - self.start_c}")
        print("")
        sys.stdout.flush()

    def run(self, rules_to_apply):
        rules_to_apply(self)

        # self.compute_hidden_intersections()
