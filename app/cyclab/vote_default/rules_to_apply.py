def exe_rules(updated):

    ## PAR DEFAUT
    # VOTE 0/5
    updated.and_tag_value_vote([("highway", "steps")], 0)
    updated.and_tag_value_vote([("highway","path")], 0)
    updated.and_tag_value_vote([("highway", "footway")], 0)
    updated.and_tag_value_vote([[("highway","footway")],[("footway", "sidewalk")]], 0)
    updated.and_tag_value_vote([[("highway","footway")],[("area", "yes")]], 0)
    updated.and_tag_value_vote([("highway","motorway")], 0)
    updated.and_tag_value_vote([("highway","motorway_link")], 0)
    updated.and_tag_value_vote([("highway","trunk")], 0)
    updated.and_tag_value_vote([("highway","trunk_link")], 0)
    updated.and_tag_value_vote([("highway","bus_guideway")], 0)
    updated.and_tag_value_vote([("highway","raceway")], 0)
    updated.and_tag_value_vote([("highway","construction")], 0)
    updated.and_tag_value_vote([("highway","proposed")], 0)
    updated.and_tag_value_vote([("highway","bridleway")], 0)
    updated.and_tag_value_vote([("bicycle","no")], 0)
    updated.and_tag_value_vote([("bicycle","mtb")], 0)
    updated.and_tag_value_vote([("bicycle:type","mtb")], 0)
    updated.and_tag_value_vote([("access","no")], 0)
    updated.and_tag_value_vote([("access","no"),[("bicycle","yes"),("bicycle","designated"),("bicycle","dismount"),("bicycle","permissive") ]], 4)
    updated.and_tag_value_vote([("access","delivery")], 0)
    updated.and_tag_value_vote([("access","official")], 0)
    updated.and_tag_value_vote([("access","private")], 0)
    updated.and_tag_value_vote([("golf_cart","yes")], 0)
    updated.and_tag_value_vote([("highway","bridleway")], 0)
    updated.and_tag_value_vote([("access","customers")], 0)
    updated.and_tag_value_vote([("access","private")], 0)
    updated.and_tag_value_vote([("access","private"),[("bicycle","yes"),("bicycle","designated"),("bicycle","dismount"),("bicycle","permissive") ]], 4)
    updated.and_tag_value_vote([("access","destination")], 0)
    updated.and_tag_value_vote([("access","destination"),[("bicycle","yes"),("bicycle","designated"),("bicycle","dismount"),("bicycle","permissive") ]], 4)

# VOTE 1/5

    updated.and_tag_value_vote([("highway","primary")], 1)
    updated.and_tag_value_vote([("highway","primary_link")], 1)
    updated.and_tag_value_vote([("highway","track"),[("tracktype","grade4")]], 1)

   # VOTE 2/5
    updated.and_tag_value_vote([("highway","track"),[("bicycle","yes"),("bicycle","designated"),("bicycle","dismount"),("bicycle","permissive") ]], 2)
    updated.and_tag_value_vote([("highway","track"),[("tracktype","grade3")]], 2)
    updated.and_tag_value_vote([("highway","secondary")], 2)
    updated.and_tag_value_vote([("highway","secondary_link")], 2)
    updated.and_tag_value_vote([("highway","primary"),[("maxspeed","50")]], 2)

    # VOTE 3/5
    updated.and_tag_value_vote([("highway","residential")], 3)
    updated.and_tag_value_vote([("highway","tertiary")], 3)
    updated.and_tag_value_vote([("highway","tertiary_link")], 3)
    updated.and_tag_value_vote([("highway","track"),[("tracktype","grade2")]], 3)
    updated.and_tag_value_vote([("highway", "footway"),[("bicycle","yes"),("bicycle","designated"),("bicycle","permissive"),("bicycle","dismount")]], 3)
    updated.and_tag_value_vote([("highway","service")], 3)
    updated.and_tag_value_vote([[("highway","footway")],[("footway", "sidewalk")],[("bicycle","yes"),("bicycle","designated"),("bicycle","dismount"),("bicycle","permissive")]], 3)
    # VOTE 4/5
    # updated.and_tag_value_vote([[("highway","service"),[("access","no")]][("bicycle","yes"),("bicycle","designated"),("bicycle","permissive"),("bicycle","dismount")]], 4)
    updated.and_tag_value_vote([("highway", "pedestrian")], 4)
    updated.and_tag_value_vote([("highway", "living_street")], 4)
    updated.and_tag_value_vote([("maxspeed", "30")], 4)
    updated.and_tag_value_vote([("zone:maxspeed", "FR:30")], 4)
    updated.and_tag_value_vote([("maxspeed", "20")], 4)
    updated.and_tag_value_vote([("highway", "track"),[("tracktype","grade1")]], 4)
    updated.and_tag_value_vote([("highway", "steps"),[("ramp:bicycle","yes")]], 4)
    updated.and_tag_value_vote([("highway", "steps"),[("ramp:stroller","yes")]], 4)
    updated.and_tag_value_vote([("highway","unclassified")], 4)


    ## VOIES VERTES
    updated.and_tag_value_vote([("highway","path"),[("bicycle","yes"),("bicycle","designated"),("bicycle","dismount"),("bicycle","permissive") ]], 3)
    updated.and_tag_value_vote([[("highway", "path")],
        [
            ("bicycle","yes"),
            ("bicycle","designated"),
            ("bicycle","permissive"),
            ("bicycle","dismount")
        ],
        [
            ("surface","unpaved"),
            ("surface","dirt"),
            ("surface","earth"),
            ("surface","ground"),
            ("surface","sand"),
            ("surface","mud"),
            ("surface","cobblestone"),
            ("surface","rock"),
            ("surface","grass"),
            ("surface","woodchips")

        ]], 2)
    updated.and_tag_value_vote([[("highway", "path")],
        [
            ("bicycle","yes"),
            ("bicycle","designated"),
            ("bicycle","permissive"),
            ("bicycle","dismount")
        ],
        [
            ("surface","gravel"),
            ("surface","pebblestone"),
            ("surface","compacted"),
            ("surface","fine_gravel")
        ]], 3)
    updated.and_tag_value_vote([[("highway", "path")],
        [
            ("bicycle","yes"),
            ("bicycle","designated"),
            ("bicycle","permissive"),
            ("bicycle","dismount")
        ],
        [
            ("surface","paved"),
            ("surface","asphalt"),
            ("surface","concrete"),
            ("surface","sett"),
            ("surface","paving_stones"),
            ("surface","concrete:lanes"),
            ("surface","concrete:plates"),
            ("surface","wood"),
            ("surface","paving_stones"),
            ("tracktype","grade1"),
            ("surface","unhewn_cobblestone"),
            ("surface","grass_paver"),
            ("surface","metal")
        ]], 4)

    updated.and_tag_value_vote([("highway","cycleway")], 4)
    updated.and_tag_value_vote([("cycleway","lane")], 4)
    updated.and_tag_value_vote([("cycleway:right","lane")], 4)
    updated.and_tag_value_vote([("cycleway:left","lane")], 4)
    updated.and_tag_value_vote([("cycleway","opposite")], 4)
    updated.and_tag_value_vote([("cycleway:right","opposite")], 4)
    updated.and_tag_value_vote([("cycleway:left","opposite")], 4)
    updated.and_tag_value_vote([("cycleway","opposite_lane")], 4)
    updated.and_tag_value_vote([("cycleway:right","opposite_lane")], 4)
    updated.and_tag_value_vote([("cycleway:left","opposite_lane")], 4)
    updated.and_tag_value_vote([("cycleway","opposite_track")], 4)
    updated.and_tag_value_vote([("cycleway:right","opposite_track")], 4)
    updated.and_tag_value_vote([("cycleway:left","opposite_track")], 4)
    updated.and_tag_value_vote([("cycleway","track")], 4)
    updated.and_tag_value_vote([("cycleway:right","track")], 4)
    updated.and_tag_value_vote([("cycleway:left","track")], 4)
    updated.and_tag_value_vote([("cycleway","opposite_track")], 4)
    updated.and_tag_value_vote([("cycleway:right","opposite_track")], 4)
    updated.and_tag_value_vote([("cycleway:left","opposite_track")], 4)
    updated.and_tag_value_vote([("cycleway","share_busway")], 4)
    updated.and_tag_value_vote([("cycleway:right","share_busway")], 4)
    updated.and_tag_value_vote([("cycleway:left","share_busway")], 4)
    # updated.and_tag_value_vote([[("cycleway", "share_busway"),[("access","no")]][("bicycle","yes"),("bicycle","designated"),("bicycle","permissive"),("bicycle","dismount")]], 4)
    # updated.and_tag_value_vote([[("cycleway:right", "share_busway"),[("access","no")]][("bicycle","yes"),("bicycle","designated"),("bicycle","permissive"),("bicycle","dismount")]], 4)
    # updated.and_tag_value_vote([[("cycleway:left", "share_busway"),[("access","no")]][("bicycle","yes"),("bicycle","designated"),("bicycle","permissive"),("bicycle","dismount")]], 4)
    updated.and_tag_value_vote([("cycleway","opposite_share_busway")], 4)
    updated.and_tag_value_vote([("cycleway:right","opposite_share_busway")], 4)
    updated.and_tag_value_vote([("cycleway:left","opposite_share_busway")], 4)
    # updated.and_tag_value_vote([[("cycleway", "opposite_share_busway"),[("access","no")]][("bicycle","yes"),("bicycle","designated"),("bicycle","permissive"),("bicycle","dismount")]], 4)
    # updated.and_tag_value_vote([[("cycleway:right", "opposite_share_busway"),[("access","no")]][("bicycle","yes"),("bicycle","designated"),("bicycle","permissive"),("bicycle","dismount")]], 4)
    # updated.and_tag_value_vote([[("cycleway:left", "opposite_share_busway"),[("access","no")]][("bicycle","yes"),("bicycle","designated"),("bicycle","permissive"),("bicycle","dismount")]], 4)
    updated.and_tag_value_vote([("cyclestreet","yes")], 4)
    updated.and_tag_value_vote([("cycleway","cyclestreet")], 4)
    updated.and_tag_value_vote([("oneway:bicycle","yes")], 4)

# VOTE 2/5 Routes et chemins pavés
    updated.and_tag_value_vote([("surface","cobblestone")], 2)
    updated.and_tag_value_vote([("surface","sett"),[("smoothness","bad"),("smoothness","very_bad"),("smoothness","horrible"),("smoothness","very_horrible"),("smoothness","impassable") ]], 2)