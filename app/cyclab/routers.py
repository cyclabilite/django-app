from rest_framework import routers
from .views.city import CityViewSet
from .views.votes import VoteViewSet, VoteReasonViewSet
from .views.osm import SegmentsViewSet, SegmentsDeletedViewSet, WaysViewSet
from .views.user import ProfileViewSet, UserViewSet

# Routers to automatically determine the URL config
router = routers.DefaultRouter()
router.register(r'cities', CityViewSet)
router.register(r'votes', VoteViewSet)
router.register(r'segments', SegmentsViewSet)
# router.register(r'segmentsDeleted', SegmentsDeletedViewSet)
# router.register(r'ways', WaysViewSet)
router.register(r'users', UserViewSet)
# router.register(r'profiles', ProfileViewSet)
router.register(r'votereasons', VoteReasonViewSet)
