const path = require("path");
const { Script } = require("vm");
var webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    name: "cyclab-map",
    mode: "development",
    watch: true,
    entry: {
      app: "./js/src/app.js",
      admin_vote_report: { import: "./js/src/admin_vote_report.js", filename: "vote-transfer.js", },
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
      }),
      new MiniCssExtractPlugin(),
    ],
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.s[ac]ss$/i,
          use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
        },
      ],
    }
};