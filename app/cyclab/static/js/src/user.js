/* jslint vars: true */
/*jslint indent: 3 */
/* global define, current_user_id, current_user_label */

/* Function pr interact with the user */
var user = function ($) {
  "use strict";
  var user_profile = null;
  var current_user = getCurrentUser();

  function init(current_user_id_arg, current_user_label_arg) {
    $("#logout_link").click(function (e) {

      ui_logout();
      user_profile = null;
      current_user = null;
      $("#logged-in-menu").addClass('hide');
      $("#login_link").show();
      $("#register-link").show();
    });

    ui_draw_menu();

    $("#log-in-link").colorbox({
      width: "358px",
      height: "358px",
      closeButton: true,
      opacity: "0.5",
      onComplete: function () {
        $("#login_form").submit(function (e) {
          e.preventDefault();
          var email = $("#id_username").val();
          var password = $("#id_password").val();
          $.ajax({
            url: "/login/",
            type: "post",
            data: {
              email: email,
              password: password,
            },
            error: function (error) {
              console.log(error);
            },
            success: function (data) {
              if (data.status_code === 200) {
                //set the data-attribute to logged in user
                $("#js-data").attr("data-user", data.user);
                current_user = getCurrentUser();

                //Set username
                $.ajax({
                  url: `api/users/${current_user}`,
                  type: "get",
                  error: function(error) {
                    console.log(error)
                  },
                  success: function (data) {
                    $("#username").removeClass('hide');
                    $("#username").html(`<p class='menu-link'><a href="/user/${data.id}">${data.username}</a></p>`)
                  }
                });
                //change the menu links
                $("#login_link").hide();
                $("#register-link").hide();
                $("#logged-in-menu").removeClass('hide');
                $.colorbox.close();
                //Show login message
                $("#main-title").addClass('hide');
                $("#username").addClass('hide');
                $("#login-message").removeClass('hide');
                setTimeout(function () {
                  $("#login-message").addClass('hide');
                  $("#main-title").removeClass('hide');
                  $("#username").removeClass('hide');
                }, 3000);
              } else {
                $("#error-msg").text(data.message)
              }
            },
          });
        });
      },
    });
  }

  //   function setCurrentUser(c_user) {
  //     current_user = c_user;
  //     ui_draw_menu();
  //   }

  // GET THE CURRENT USER
  function getCurrentUser() {
    const user_data = document.getElementById("js-data");
    if (user_data.dataset.user != "None") {
      return user_data.dataset.user;
    } else {
      return null;
    }
  }

  function ui_login() {
    $("#login_link").hide();
    $("#register-link").hide();
    $("#logged-in-menu").removeClass('hide');
    $("#username").removeClass('hide');
  }

  function ui_logout() {
    $("#login_link").show();
    $("#logged-in-menu").addClass('hide');
    $("#username").addClass('hide');
  }

  function ui_draw_menu() {
    if (current_user) {
      ui_login();
    } else {
      ui_logout();
    }
  }

  /*
   * Apply a callback to get the current user data (if the user is logged)
   */
  function getCurrentData(callback) {
    if (!user_profile && current_user) {
      $.ajax({
        url: "api/users/" + current_user,
        cache: false,
        success: function (output_json) {
          user_profile = output_json;
          callback(user_profile);
        },
      });
    } else {
      callback(user_profile);
    }
  }

  return {
    init: init,
    getCurrentUser: getCurrentUser,
    getCurrentData: getCurrentData,
  };
};

export default user;
