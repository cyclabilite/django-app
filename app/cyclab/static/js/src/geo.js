/* jslint vars: true */
/*jslint indent: 3 */
/* global define */

var geo = function (L) {
  "use strict";
  var geojson = {};

  geojson.getLatLngFromPoint = function (p) {
    return getLatlngFromCoordinates(p.geometry.coordinates);
  };

  /* Transform coordinates to a  Leaflet.latLng object
   * @param {coodinates (array of 2 elements : 0 ->lon, 1-> lat)} c The coordinates
   * @return {Leaflet.latLng} The Leaflet.latLng object
   */
  function getLatlngFromCoordinates(c) {
    return L.latLng(c[1], c[0]);
  }

  /* Get the length of a linestring
   * @param {Linestring} line The linestring
   * @return {number} the length of the linestring (in meter?)
   */
  function getLength(line) {
    var coordinates = line.coordinates;
    var length = 0,
      i;
    var latlng0 = getLatlngFromCoordinates(coordinates[0]);

    for (i = 1; i < coordinates.length; i++) {
      var latlng1 = getLatlngFromCoordinates(coordinates[i]);
      length += latlng0.distanceTo(latlng1);
      latlng0 = latlng1;
    }

    return length;
  }

  /*
   * Get the Leaflet.latLng the position at a percentage/length on a linestring
   * @param {LineString} line The linestring
   * @param {Node} reference_coord The coord from where the  percentage/length is given
   * @param {float} percentage_or_length The position on the linestring in percentage (in [0,1]) or in length (>= 1)
   * (if the lentgh is greater than the line length, the end of the line is returned)
   * @return {Leafletjs.LatLng} The position on the line at the percentage percentage
   */
  function getPosition(line, reference_coord, percentage_or_length) {
    var coordinates = line.coordinates;
    var line_length = getLength(line);
    var latlng0 = getLatlngFromCoordinates(coordinates[0]);
    var latlng1;
    var next_coord = 2;
    var distance_1_to_position;
    var distance_1_to_0;
    var line_position;
    var current_length;

    var reference_coord_latlng = getLatlngFromCoordinates(reference_coord);
    if (
      reference_coord_latlng.lat != latlng0.lat ||
      reference_coord_latlng.lng != latlng0.lng
    ) {
      coordinates = line.coordinates.slice(0); //copy
      coordinates.reverse();
      latlng0 = getLatlngFromCoordinates(coordinates[0]);
    }

    latlng1 = getLatlngFromCoordinates(coordinates[1]);
    current_length = latlng0.distanceTo(latlng1);

    if (percentage_or_length < 1) {
      line_position = line_length * percentage_or_length;
    } else {
      line_position = percentage_or_length;
    }

    while (current_length < line_position && next_coord < coordinates.length) {
      latlng0 = latlng1;
      latlng1 = getLatlngFromCoordinates(coordinates[next_coord]);
      current_length += latlng0.distanceTo(latlng1);
      next_coord++;
    }

    // if percentage_or_length is a length and is greater than global_length
    if (current_length < line_position) {
      return latlng1;
    }

    distance_1_to_position = current_length - line_position;
    distance_1_to_0 = latlng0.distanceTo(latlng1);

    return L.latLng([
      latlng1.lat -
        (latlng1.lat - latlng0.lat) *
          (distance_1_to_position / distance_1_to_0),
      latlng1.lng -
        (latlng1.lng - latlng0.lng) *
          (distance_1_to_position / distance_1_to_0),
    ]);
  }

  return {
    getPosition: getPosition,
    geojson: geojson,
  };
};

export default geo;
