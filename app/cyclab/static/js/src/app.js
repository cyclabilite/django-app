/*jshint esversion: 6*/

import $ from "jquery";
import cyclab_map from "./cyclab_map.js";
import user from "./user.js";
import entity from "./entity.js";
import geo from "./geo.js";

const main_css = require('../../css/cyclab-main.scss');

const select2 = require("select2");
const leaflet = require("leaflet");
const routie = require("routie-2");
const colorbox = require("jquery-colorbox");
const mustache = require("mustache");

const app = (function () {
  function init(cities, default_city, current_user) {
    //set CSRF token for all future ajax calls
    $.ajaxSetup({
      beforeSend: function (xhr, settings) {
        function getCookie(name) {
          var cookieValue = null;
          if (document.cookie && document.cookie != "") {
            var cookies = document.cookie.split(";");
            for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == name + "=") {
                cookieValue = decodeURIComponent(
                  cookie.substring(name.length + 1)
                );
                break;
              }
            }
          }
          return cookieValue;
        }
        if (
          !(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))
        ) {
          // Only send the token to relative URLs i.e. locally.
          xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
        }
      },
    });

    //initialization of the map
    let cyclab_map_m = cyclab_map($, leaflet, select2);

    let cities_p = {};

    cities.forEach(function (c) {
      cities_p[c.city_name] = [c.lon, c.lat, c.zoom_level];
    });

    cyclab_map_m.setCities(cities);

    if (default_city) {
      cyclab_map_m.setDefaultCity(default_city);
    }

    if (default_city) {
      cyclab_map_m.goToCity(default_city);
    } else {
      cyclab_map_m.goToCity(null);
    }

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        cyclab_map_m.gotToLonLatLevel(
          position.coords.latitude,
          position.coords.longitude,
          17
        );
      });
    }

    var user_m = user($);

    if (current_user) {
      console.log("Set Cur User");
      user_m.setCurrentUser(current_user);
    }

    var geo_m = geo(leaflet);

    cyclab_map_m.addLegend();

    var entity_m = entity(
      $,
      leaflet,
      mustache,
      cyclab_map_m,
      user_m,
      geo_m,
      select2
    );
    entity_m.init();

    $(document).ready(function () {
      routie({
        "map/:lat/:lon/:zoom?": cyclab_map_m.gotToLonLatLevel,
        ":city/:zoom?": cyclab_map_m.goToCity,
        "": function () {
          cyclab_map_m.goToCity(null);
        },
      });

      $("#town_choice_link").colorbox({
        inline: true,
        width: "358px",
        height: "358px",
      });

      user_m.init();
    });
  }
  return { init: init };
})();

document.addEventListener("DOMContentLoaded", function () {
  $.ajax({
    type: "GET",
    url: "/api/cities/",
    success: function (response) {
      var cities = response;
      var default_city = response.filter((city) => city.is_default === true)[0][
        "city_name"
      ];
      var user = "";
      app.init(cities, default_city, user);
    },
    error: function (error) {
      console.log(error);
    },
  });
});
