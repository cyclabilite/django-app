/* jslint vars: true */
/*jslint indent: 3 */
/* global define */
var entity = function ($, L, Mustache, cyclab_map, user, geo, select2) {
  "use strict";

  var LAYER_ENTITIES = "entities";
  var LAYER_VOTE = "vote";

  var vote_mode = false;
  var vote_reasons = null;

  /*
   * Get first reason at first call and send it to the callback
   */
  function getVoteReasons(callback) {
    if (!vote_reasons) {
      $.ajax({
        url: "api/votereasons/",
        cache: false,
        success: function (output_json) {
          vote_reasons = output_json;
          callback(vote_reasons);
        },
      });
    } else {
      callback(vote_reasons);
    }
  }

  function load(success_callback, fail_callback) {
    var map = cyclab_map.getMap();

    $.get(
      "/bboxcyclabsegments/?bbox=" + map.getBounds().toBBoxString(), // to do remplacer par cyclab_map.getBBoxString()
      function (entities) {
        var options = {
          style: function (feature) {
            return {
              color: cyclab_map.getColor(feature),
              weight: 10,
              opacity: 0.5,
            };
            // marche pas car c'est pour segment et pas ways
          },
          pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, {
              color: cyclab_map.getColor(feature),
            });
          },
          onEachFeature: function (feature, layer) {
            layer.off("click");
            layer.on("click", displayPopup);
          },
          weight: 8,
        };

        cyclab_map.addGeoJsonLayer(LAYER_ENTITIES, entities, options);
      }
    )
      .done(function () {
        if (success_callback) {
          success_callback();
        }
      })
      .fail(function () {
        if (fail_callback) {
          fail_callback();
        }
      });
  }

  function displayPopup(e) {
    // load the details of the entity (pour results + vote)
    // afficher les résultats dnas popup
    var entity = e.target.feature.properties;

    var popup_content;
    var vote_results = "";

    var url_entity_get_details = "api/segments/" + entity.pk;

    $.ajax({
      url: url_entity_get_details,
      cache: false,
      success: function (detailed_entity) {

        // console.log("segment", detailed_entity);
        var votes = detailed_entity.votes;

        if (votes.length === 0) {
          vote_results = $("#no_vote").text();
        } else {
          $.each(votes, function (i, v) {
            vote_results += Mustache.render(
              $("#result-vote-item-list-tpl").text(),
              v
            );
          });
        }

        popup_content = Mustache.render($("#result-tpl").text(), {
          vote_average: detailed_entity.vote_average ? parseFloat(detailed_entity.vote_average.toFixed(2)) : null,
          content: vote_results,
          entity_id: entity.pk,
        });

        cyclab_map.addPopup(e.latlng, popup_content, {
          offset: cyclab_map.getPopupOffsetFor(detailed_entity),
        });

        $("#display-vote-form-" + entity.pk).click(function () {
          displayVotePopup(detailed_entity);
        });
      },
    });
  }

function displayVotePopup(det_entity) {
    getVoteReasons(function (vote_reasons) {
      user.getCurrentData(function (user_data) {
        var popup_content;

        var i;
        var timestamp;
        var now = new Date();
        var now_hours = now.getHours();
        var now_minutes = now.getMinutes();
        var id_vote_form;
        var selector_vote_form;
        var template_args;

        vote_mode = true;

        if (user_data) {
          timestamp = new Date().getTime();
          id_vote_form = "vote-form-" + timestamp;
          selector_vote_form = "#" + id_vote_form;

          if (now_hours <= 9) {
            now_hours = "0" + now_hours;
          }

          if (now_minutes <= 9) {
            now_minutes = "0" + now_minutes;
          }

          template_args = {
            id_vote_form: id_vote_form,
            id: det_entity.id,
            user_id: user_data.id,
            vote_reasons: vote_reasons,
            entity: det_entity.type,
            current_datetime: new Date().toJSON().slice(0,19)
          };
          //open vote form from template when 'voter' is clicked

          popup_content = Mustache.render(
            $("#vote-form_segment").text(),
            template_args
          );

          $(function(){
            $("input[name='star']").change(function (event) {
              $("#vote_reasons").html("");
              if(event.target.value > 0 && event.target.value < 4) {
                const negatives = vote_reasons.filter((reason) => reason.reason_value == 'Negative')
                for (let i = 0; i < negatives.length; i++) {
                  $("#vote_reasons").append(`<option value="${ negatives[i].id }">${ negatives[i].description }</option>`);
                }
              } else {
                const positives = vote_reasons.filter((reason) => reason.reason_value == 'Positive')
                for (let i = 0; i < positives.length; i++) {
                  $("#vote_reasons").append(`<option value="${ positives[i].id }">${ positives[i].description }</option>`);
                }
              }
              return false
            });
          });
        } else {
          popup_content = $("#please_log_in").text();
        }


        if (det_entity.type == "segment") {
          var lat_lng_A, lat_lng_B;

          var selected_way_segment_layers_number = 0;
          var selected_way_segments_nbr =
            det_entity.way.properties.segments.features.length;
          var way_segment_layers = [];

          var options = {
            onEachFeature: function (feature, layer) {
              layer.feature.properties.selected = true;
              selected_way_segment_layers_number =
                selected_way_segment_layers_number + 1;

              var onClickAction = function (e) {
                var layer = e.target;
                if (e.type == "click" && !layer.feature.properties.selected) {
                  layer.feature.properties.selected = !layer.feature.properties
                    .selected;
                  selected_way_segment_layers_number =
                    selected_way_segment_layers_number + 1;
                } else if (
                  e.type == "contextmenu" &&
                  layer.feature.properties.selected
                ) {
                  layer.feature.properties.selected = !layer.feature.properties
                    .selected;
                  selected_way_segment_layers_number =
                    selected_way_segment_layers_number - 1;
                }

                if (
                  selected_way_segment_layers_number === selected_way_segments_nbr
                ) {
                  $(".vote_way").show();
                  $(".vote_way_segment").hide();
                } else {
                  $(".vote_way").hide();
                  $(".vote_way_segment").show();
                }

                for (i = 0; i < way_segment_layers.length; i++) {
                  // console.log('way_segment', way_segment_layers[i])
                  if (way_segment_layers[i].feature.properties.selected) {
                    way_segment_layers[i].setStyle({
                      weight: 10,
                      opacity: 0.5,
                      color: cyclab_map.getColor(det_entity),
                    });
                  } else {
                    way_segment_layers[i].setStyle({
                      weight: 10,
                      color: cyclab_map.unselected_color,
                      opacity: 0.5,
                    });
                  }
                }
              };

              way_segment_layers.push(layer);

              layer.on({
                contextmenu: function (e) {
                  onClickAction(e);
                },
                click: function (e) {
                  onClickAction(e);
                },
              });
            },
            style: function () {
              // console.log('det_entity', det_entity)
              return {
                weight: 10,
                opacity: 0.5,
                color: cyclab_map.getColor(det_entity),
              };
            },
          };

          var way_segments = det_entity.way.properties.segments.features;

          /* Calcul de point debut du way et fin */
          var way_segments_0 = way_segments[0];
          var way_segments_n = way_segments[0];

          way_segments.forEach(function (s) {
            if (way_segments_0.id > s.id) {
              way_segments_0 = s;
            }

            if (way_segments_n.id < s.id) {
              way_segments_n = s;
            }
          });

          lat_lng_A = geo.geojson.getLatLngFromPoint(
            way_segments_0.properties.nodestart
          );
          lat_lng_B = geo.geojson.getLatLngFromPoint(
            way_segments_n.properties.nodeend
          );

          cyclab_map.hideLayer(LAYER_ENTITIES);

          cyclab_map.addGeoJsonLayer(
            LAYER_VOTE,
            det_entity.way.properties.segments,
            options
          );

          cyclab_map.updatePopupContent(popup_content); // TODO AMELIORER

          cyclab_map.showMarker("A", lat_lng_A);
          cyclab_map.showMarker("B", lat_lng_B);

          $(selector_vote_form + "-direction-from-to-div").hide();

          $(selector_vote_form + "-direction").select2();
          $(selector_vote_form + "-direction-from").select2();
          $(selector_vote_form + "-direction-to").select2();
          $(selector_vote_form + "-profile").select2();

          $(".vote_way_segment").hide();
          $(".vote_way").show();

          $(selector_vote_form).submit(function (e) {
            e.preventDefault();

            var selected_segments = [];
            var segments_features = det_entity.way.properties.segments.features;

            for (i = 0; i < segments_features.length; i++) {
              if (segments_features[i].properties.selected) {
                selected_segments.push(segments_features[i].id);
              }
            }

            const voting = document.querySelector('input[name="star"]:checked')

            if (voting === null) {
              $(selector_vote_form).hide();
              $(selector_vote_form + "-error-vote-value").show();
            } else {
              addVoteFor(selected_segments, selector_vote_form);
            }


            return false;
          });
        }
      })
    });
  }

  /*
   * Add some vote for a given list of entities id
   * @param {int list} entity_id_list The id list of the entity for which we add the vote
   * @param {String} selector_voting_form The selector of the voting form
   * @param TODO : target
   * @return No return
   */
  function addVoteFor(entity_id_list, selector_voting_form) {
    if (entity_id_list.length === 0) {
      $(selector_voting_form).hide();
      $(selector_voting_form + "-success").show();
      setTimeout(stopVote, 2500);
    } else {
      var entity_id = entity_id_list.pop();
      var url = "api/votes/";

      var profile_id = document.getElementById("profile").value;
      var user_id = document.getElementById("user").value;
      var vote_value = document.querySelector('input[name="star"]:checked').value;
      var direction = document.getElementById("segm_direction").value;
      var vote_reason = document.getElementById("vote_reasons").value;

      $.ajax({
        type: "POST",
        url: url,
        data: {
          segment: entity_id,
          direction: direction,
          vote_reason: vote_reason,
          vote_value: vote_value,
          profile: profile_id,
          user: user_id,
        },
        success: function (data) {
          $.ajax({
            type: "GET",
            url: "api/segments/" + data.segment + "/",
            success: function (s_data) {
              cyclab_map.updateLayer(
                LAYER_ENTITIES,
                function (l) {
                  if (l.feature.properties.pk == entity_id) {
                    return true;
                  }
                },
                function (l) {
                  l.feature.properties.vote_average = s_data.vote_average;
                  l.feature.properties.vote_number = s_data.vote_number;
                  l.setStyle({
                    color: cyclab_map.getColor(l.feature),
                  });
                }
              );
            },
            error: function (error) {
              console.log(error);
            },
          });

          // TODO mettre a jour la couleur du layer segemnt
          if (data.id) {
            addVoteFor(entity_id_list, selector_voting_form); // continue to vote in the list
          } else {
            $(selector_voting_form + "-error-default").show();
            $(selector_voting_form + "-error-custom").hide();
            setTimeout(stopVote, 2500);
          }
        },
        error: function (response) {
          $(selector_voting_form + "-error-custom").text(
            response.responseJSON[0]
          );
          $(selector_voting_form + "-error-custom").show();
        },
      });
    }
  }

  function stopVote() {
    cyclab_map.showLayer(LAYER_ENTITIES);
    cyclab_map.hideLayer(LAYER_VOTE);
    cyclab_map.closePopup();

    vote_mode = false;

    cyclab_map.hideMarker("A");
    cyclab_map.hideMarker("B");
  }

  /*
   * Display the buttons regarding to the zoom level and the entity selection
   */
  function updateActionButtons(map) {
    if (map.getZoom() < cyclab_map.display_entity_min_zoom_level) {
      $("#zoom-message").css("display", "inline");
    } else {
      $("#zoom-message").hide();
    }
  }

  function loadData() {
    if (
      cyclab_map.getMap().getZoom() >=
        cyclab_map.display_entity_min_zoom_level &&
      !vote_mode
    ) {
      $("#loading-message").css("display", "inline");
        load(loadingSuccess, loadingError);
    }
  }

  function loadingSuccess() {
    $("#loading-message").hide();
  }

  function loadingError() {
    $("#loading-message").hide();
    $("#error-message").css("display", "inline");

    setTimeout(function () {
      $("#error-message").hide();
    }, 5000);
  }

  function init() {
    cyclab_map.addEvent("zoomend", function (e) {
      updateActionButtons(e.target);
    });

    cyclab_map.addEvent("moveend", function () {
      loadData();
    });

    cyclab_map.addEvent("popupclose", function (e) {
      stopVote();
      return false;
    });

    updateActionButtons(cyclab_map.getMap());
    loadData();
  }

  return {
    load: load,
    stopVote: stopVote,
    init: init,
  };
};

export default entity;
