��    -      �  =   �      �     �  E   �     0     7     =     P     U     \     {     �     �      �     �     �     �     �     �     �     �                    -     G     U     a      g     �  .   �     �     �      �  a        �  =   �  9   �  +     (   E     n     u     �  
   �  	   �     �  A  �  	   �  W   �     U	     ]	     d	     �	  	   �	     �	     �	     �	     �	  '   �	     �	     
     
     4
     <
  
   E
  	   P
     Z
     r
  !   ~
  $   �
     �
     �
     �
     �
       G   (  %   p  ,   �  ,   �  g   �     X  F   r  A   �  &   �  ,   "     O     V     e     k     s     w                                               !       )                %   $           -                 #   (   
                       ,                   &   +             	         '      "               *        About us Activation link is invalid! Please contact us at info@droitauvelo.org Cancel Close Create new profile Data Delete Edit your personal information Female Help Log In Login first to see your profiles Lost password Male Please activate your account Profile Profiles Register Return home Return to map Save Save and add another Save and continue editing Save and view Save as new Speed This account is no longer active Type of rider Username or password are incorrect, try again. Users must have a first name Users must have a last name Users must have an email address You have succesfully registered. Please confirm your account through the email that has been sent You have successfully logged in You should activate your account using the link sent by email Your account has been confirmed and you are now logged in Your details have been succesfully modified Your password has been saved succesfully active date joined email address first name last name staff status Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 À propos Le lien d'activation n'est pas valide ! Veuillez nous contacter à info@droitauvelo.org Annuler Fermer Crééer une nouvelle profile Données Supprimer Modifier votre info personelle Femme Manuel d'aide Se connecter Se connecter avant de voir vos profiles Mot de passe perdu Homme Veuillez activer votre compte Profile Profiles S'inscrire Retourner Retourner vers la carte Enregistrer Enregistrer et ajouter un nouveau Enregistrer et continuer de modifier Enregistrer et voir Enregistrer comme nouveau Vitesse Ce compte n'est plus active Type de cycliste Votre nom d'utilisateur ou mot de passe n'est pas correcte, réessayer. Utilisateurs doivent avoir un prénom Utilisateurs doivent avoir un nom de famille Utilisateurs doivent avoir une adresse email Vous vous êtes inscrit avec succès. Veuillez confirmer votre compte via le e-mail qui a été envoyé Vous vous êtes connecté Vous devez activer votre compte en utilisant le lien envoyé par email Votre compte a été confirmé et vous êtes maintenant connecté Votre modification a été enregistré Ton nouveau mot de passe a été enregistré Active Date de jointe Email Prénom Nom Statut staff 