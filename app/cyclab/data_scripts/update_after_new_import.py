from subprocess import run as subprocess_run
from subprocess import PIPE, Popen
import sys
import db_fct

# https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
# see https://gist.github.com/vaultah/d63cb4c86be2774377aa674b009f759a

import sys, importlib
from pathlib import Path

def import_parents(level=1):
    global __package__
    file = Path(__file__).resolve()
    parent, top = file.parent, file.parents[level]

    sys.path.append(str(top))
    try:
        sys.path.remove(str(parent))
    except ValueError: # already removed
        pass

    __package__ = '.'.join(parent.parts[len(top.parts):])
    importlib.import_module(__package__) # won't be needed after that

if __name__ == '__main__' and __package__ is None:
    import_parents(level=3) # N = 3


from .timelogger import TaskTimeLogger
from .cyclab_db import CyclabDB


if __name__ == '__main__':

    if __name__ == '__main__' and __package__ is None:
        import_parents(level=3) # N = 3

    from .. import settings

    log = TaskTimeLogger()

    if len(sys.argv) <= 1:
        print("usage : python import.py path_to_osm_pbf_file [path to osmosis executable]")
        sys.exit()

    osm_pbf_input = sys.argv[1]

    if len(sys.argv) >= 3:
        osmosis_exec = sys.argv[2]
    else:
        osmosis_exec = "osmosis"

    log.start("DB CONNECTION")
    db = CyclabDB(settings,log)
    cur = db.connect()
    log.end_current_task()

    log.start("REMOVING CONSTRAINT")
    cur.execute("ALTER TABLE cyclab_intersection_paths DROP CONSTRAINT IF EXISTS FK_cyclab_intersection_paths_node_id")
    cur.execute("ALTER TABLE cyclab_segments DROP CONSTRAINT IF EXISTS FK_cyclab_segments_way_id")
    cur.execute("ALTER TABLE cyclab_segments DROP CONSTRAINT IF EXISTS FK_cyclab_segments_nodeStart_id")
    cur.execute("ALTER TABLE cyclab_segments DROP CONSTRAINT IF EXISTS FK_cyclab_segments_nodeEnd_id")
    log.end_current_task()

    log.start("REMOVING OLD OSM DATA")
    db.exec_file('sql/drop_osm_db.sql') 
    db.exec_file('sql/00100-pgsnapshot_schema_0.6.sql')
    db.exec_file('sql/00120-pgsnapshot_schema_0.6_actions.sql') 
    db.exec_file('sql/00130-pgsnapshot_schema_0.6_changes.sql')
    db.exec_file('sql/00140-pgsnapshot_schema_0.6_bbox.sql')
    db.exec_file('sql/00150-pgsnapshot_schema_0.6_linestring.sql')
    log.end_current_task()

    log.start("DISCONNECT DB (avoid deadlock)")
    db.disconnect()
    log.end_current_task()

    log.start("IMPORT OSM DATA  (using osmosis)")
    subprocess_run([
        osmosis_exec, "--read-pbf", osm_pbf_input,
        "--tf", "accept-ways", "highway=*",
        "--tf", "reject-ways", "highway=motorway,motorway_link,trunk,trunk_link,bus_guideway,raceway,steps",
        "--tf", "reject-ways", "bicycle=no",
        "--tf", "reject-ways", "access=private",
        "--tf", "reject-relations", "--used-node", "outPipe.0=highway",
        "--read-pbf", osm_pbf_input, "--tf", "reject-nodes",
        "--tf", "reject-ways", "--tf", "accept-relations", "network=lcn,rcn,ncn", "outPipe.0=cycle_network",
        "--merge", "inPipe.0=highway", "inPipe.1=cycle_network",
        "--write-pgsql", f"host={settings.DB_HOST}:{settings.DB_PORT}",
        f"database={settings.DB_NAME}", f"user={settings.DB_USER}", f"password={settings.DB_PWD}"],
        check=True)
    log.end_current_task()

    log.start("RECONNECT DB")
    db = CyclabDB(settings,log)
    cur = db.connect()
    log.end_current_task()

    log.start("Pimp the OSM DB (add extra data that we will use")
    db.exec_file('sql/pimp_osm_db.sql')
    db.commit()
    log.end_current_task()

    log.start("RENAME cyclab_segment to old_cyclab_segments")
    cur.execute('ALTER TABLE cyclab_segments RENAME TO old_cyclab_segments;')
    db.commit()
    log.end_current_task()

    log.start("CREATE TABLE temp_nodes (for recomputing segments)")
    cur.execute("DROP TABLE IF EXISTS tmp_nodes")
    cur.execute("CREATE TABLE tmp_nodes (\
                         id BIGINT NOT NULL, \
                         ways_nbr BIGINT NOT NULL, \
                         PRIMARY KEY(id)); \
                         \
                         INSERT INTO tmp_nodes (id, ways_nbr) \
                         SELECT n.id AS id, COUNT(w.id) AS ways_nbr \
                             FROM nodes AS n \
                             JOIN ways as w ON n.id = ANY(w.nodes) \
                             GROUP BY n.id \
                             HAVING  COUNT(w.id) > 1;")
    db.commit()
    log.end_current_task()

    log.start("UPDATING NODES TABLE") 
    cur.execute("UPDATE nodes AS n \
    SET ways_nbr = tmp.ways_nbr \
    FROM tmp_nodes as tmp WHERE n.id = tmp.id AND tmp.ways_nbr > 1")
    db.commit()
    log.end_current_task()

    log.start("BUFFERING NODES")
    cur.execute("SELECT id, geom, ways_nbr FROM nodes")
    buff_nodes = {}
    
    for row in cur:
        buff_nodes[row[0]] = (row[1], row[2])
    log.end_current_task()

    log.start('RECREATE cycab_segments_tble')
    cur.execute("""
        CREATE TABLE cyclab_segments (
            id BIGINT NOT NULL DEFAULT nextval('cyclab_segments_id_seq'),
            way_id BIGINT DEFAULT NULL,
            nodeStart_id BIGINT DEFAULT NULL,
            nodeEnd_id BIGINT DEFAULT NULL,
            geom geometry(LineString,4326),
            vote_default integer DEFAULT 3,
            vote_average float DEFAULT NULL,
            vote_number integer DEFAULT 0 NOT NULL,
            averagedata json NOT NULL DEFAULT '{}'::json,
	        vote_average_both float NULL,
	        vote_average_ba float NULL,
	        vote_average_ab float NULL,
            PRIMARY KEY(id))
        ;
        """);

    for req in [
        "drop INDEX cyclab_segment_geom;",
        "drop INDEX idx_26cedb29490b3fc2;",
        "drop INDEX idx_26cedb297682f146;",
        "drop INDEX idx_26cedb298c803113;",
        "CREATE INDEX cyclab_segment_geom ON public.cyclab_segments USING gist (geom);",
        "CREATE INDEX idx_26cedb29490b3fc2 ON public.cyclab_segments USING btree (nodestart_id);",
        "CREATE INDEX idx_26cedb297682f146 ON public.cyclab_segments USING btree (nodeend_id);",
        "CREATE INDEX idx_26cedb298c803113 ON public.cyclab_segments USING btree (way_id);"]:
        cur.execute(req)
    log.end_current_task()


    log.start("ADDING SEGMENTS")
    cur.execute("SELECT id, nodes, tags FROM ways")
    cur2 = db.new_cursor()

    for row in cur:
        way_id = row[0]
        way_nodes = row[1]
        way_tags = row[2]
        current_segment = [buff_nodes[way_nodes[0]][0]]
        nodestart_id = way_nodes[0]
        for n in way_nodes[1:]:
            current_segment.append(buff_nodes[n][0])
            nodeend_id = n
            if buff_nodes[n][1] > 1:
                db_fct.db_add_segment(cur2, way_id, nodestart_id, nodeend_id, current_segment)
                current_segment = [buff_nodes[n][0]]
                nodestart_id = n
        if len(current_segment) > 1:
            db_fct.db_add_segment(cur2, way_id, nodestart_id, nodeend_id, current_segment)

    db.commit()
    log.end_current_task()

    log.start("DROP CONSTRAINT")
    # drop constraint
    cur.execute("ALTER TABLE cyclab_votesegment DROP CONSTRAINT IF EXISTS cyclab_vote_segment_segment_id_00e9817d_fk_cyclab_segments_id;")
    log.end_current_task()


    log.start("List of segment to keep (used in intersection path")
    cur.execute("""
        DROP TABLE IF EXISTS temp_segments_in_intersection_paths;
    """)
    cur.execute("""
        CREATE TABLE temp_segments_in_intersection_paths AS
        (
            (SELECT segmentstart_id as id FROM cyclab_intersection_paths where segmentstart_id is not NULL )
        UNION 
            (SELECT segmentend_id as id FROM cyclab_intersection_paths where segmentend_id  is not NULL)
        );
    """)
    log.end_current_task()
    db.commit()

    # FIRST GROS NETTOYAGE
    log.start("NETTOYAGE OLD_SEGMENT avec vote NULL")
    cur.execute("""
        DELETE FROM old_cyclab_segments
            WHERE
                vote_number = 0
            and 
                id not in (select * FROM temp_segments_in_intersection_paths);
    """)
    log.end_current_task()

    log.start("CREATE TEMP TABLE MATCHING")
    cur.execute("""
        DROP TABLE IF EXISTS old_new_matching_segment
    """)
    cur.execute("""
        CREATE TEMPORARY TABLE old_new_matching_segment AS
        SELECT
            cs_new.way_id as way_id,
            cs_new.nodestart_id as nodestart_id,
            cs_new.nodeend_id as nodeend_id,
            cs_new.id as new_seg_id,
            cs_old.id as old_seg_id
        FROM
            cyclab_segments as cs_new
        JOIN
            old_cyclab_segments as cs_old ON cs_new.way_id = cs_old.way_id AND cs_new.nodestart_id = cs_old.nodestart_id AND  cs_new.nodeend_id = cs_old.nodeend_id 
            WHERE cs_old.vote_number > 0;
    """)
    log.end_current_task()
    db.commit()

    # transfer des votes
    log.start("CHANGE VOTE_SEGMENT_ID (VOTE ON EXISTING SEG")
    cur.execute("""SELECT
            way_id,
            nodestart_id,
            nodeend_id,
            new_seg_id,
            old_seg_id
        FROM 
            old_new_matching_segment;
    """)

    for (way_id, nodestart_id, nodeend_id, new_seg_id, old_seg_id) in cur:
        cur2 = db.new_cursor()
        # print(f"UPDATE cyclab_votesegment SET segment_id =  {new_seg_id} WHERE segment_id = {old_seg_id};")
        cur2.execute(f"UPDATE cyclab_votesegment SET segment_id =  {new_seg_id} WHERE segment_id = {old_seg_id};")
        # print(row)
    log.end_current_task()

    # NETTOYAGE OLD_SEGMENT VOTE MATCHING
    log.start("NETTOYAGE OLD_SEGMENT VOTE MATCHING")
    cur.execute("""
        DELETE FROM old_cyclab_segments
        WHERE
            id in (SELECT old_seg_id FROM old_new_matching_segment)
        AND 
            id not in (select * from temp_segments_in_intersection_paths);
    """)


    # Transfer vote vers deleted
    log.start("SET COPY VOTE AS DELETED_VOTES (AND REMOVE IT FROM old_cyclab_segments")
    cur.execute("""
        INSERT INTO public.cyclab_deletedvotesegment
            (id, vote_value, vote_creation_time, "direction", profile_id, segment_id, user_id, vote_reason_id, latest)
        SELECT 
            id, vote_value, vote_creation_time, "direction", profile_id, segment_id, user_id, vote_reason_id, latest
        FROM 
            cyclab_votesegment
        WHERE 
            segment_id in (select id from old_cyclab_segments)
        ;
    """)

    cur.execute("""
        DELETE FROM
            cyclab_votesegment
        WHERE 
            segment_id in (select id from old_cyclab_segments)
        ;
    """) # TODO ET LES AUTRES ?
    log.end_current_task()

    # REcopier vers deleted_signal_segment les old_cyclab_segments
    log.start("RECOPIER vers deleted segment")
    cur.execute("""
        INSERT INTO public.deleted_cyclab_segments (
            id, way_id, nodestart_id, nodeend_id, geom, vote_default, 
            vote_average, vote_number, averagedata,
            vote_average_both, vote_average_ba, vote_average_ab
        )
        SELECT
            id, way_id, nodestart_id, nodeend_id, geom, vote_default,
            vote_average, vote_number, averagedata, 
            vote_average_both, vote_average_ba, vote_average_ab
        FROM public.old_cyclab_segments
        ;
    """)
    log.end_current_task()


    #RECERER FK  CYCLAB_SEGMENTS_VOTES VERS CYCLAB_SEGMENTS
    log.start("RECRATE FK")
    # TODO
    log.end_current_task()

    # DROP old_   DROP TABLE  old_cyclab_segments;
    log.start("DROP old_cyclab_segments TABLE")
    cur.execute("alter table cyclab_intersection_paths  DROP CONSTRAINT IF EXISTS fk_286464817a2ced74;")
    cur.execute("alter table cyclab_intersection_paths   DROP CONSTRAINT IF EXISTS fk_28646481b07a3b52;")

    cur.execute("DROP VIEW IF EXISTS cyclab_segments_all;");
    cur.execute("DROP TABLE IF EXISTS  old_cyclab_segments;");

    cur.execute("""
        CREATE OR REPLACE VIEW cyclab_segments_all AS (
            SELECT id, way_id, nodestart_id, nodeend_id, geom, vote_default, vote_average, vote_number, averagedata, vote_average_both, vote_average_ba, vote_average_ab FROM cyclab_segments
        ) UNION ALL  (
            SELECT id, way_id, nodestart_id, nodeend_id, geom, vote_default, vote_average, vote_number, averagedata, vote_average_both, vote_average_ba, vote_average_ab FROM deleted_cyclab_segments
        );
    """);

    cur.execute("""
        CREATE OR REPLACE VIEW ways_all AS (
        WITH req1 AS
        ((
        SELECT id, tstamp, linestring, changeset_id, version, tags, user_id,  vote_default, vote_number, vote_average, nodes FROM ways 
        ) UNION ALL  (
                    SELECT id, tstamp, linestring, changeset_id, version, tags, user_id,  vote_default, vote_number, vote_average, nodes FROM deleted_ways 
                )
        ),
        req_with_row_nb AS
        (
        SELECT *,
                ROW_NUMBER() OVER (PARTITION BY id ORDER BY tstamp DESC) AS rn
        FROM req1
        )
        SELECT *
        FROM req_with_row_nb WHERE rn = 1
        );
    """);

    log.end_current_task()

    log.start("remettre CONSTRAINT")
    # drop constraint
    # cur.execute("ALTER TABLE cyclab_votesegment DROP CONSTRAINT cyclab_vote_segment_segment_id_00e9817d_fk_cyclab_segments_id;")
    log.end_current_task()

    db.disconnect()
    log.end()
