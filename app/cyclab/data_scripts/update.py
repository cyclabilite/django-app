# -*- coding: utf-8 -*-
import sys
import time
import subprocess
from xml.etree import ElementTree
import psycopg2
import db_fct

# https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
# see https://gist.github.com/vaultah/d63cb4c86be2774377aa674b009f759a

import sys, importlib
from pathlib import Path


def import_parents(level=1):
    global __package__
    file = Path(__file__).resolve()
    parent, top = file.parent, file.parents[level]
    
    sys.path.append(str(top))
    try:
        sys.path.remove(str(parent))
    except ValueError: # already removed
        pass

    __package__ = '.'.join(parent.parts[len(top.parts):])
    importlib.import_module(__package__) # won't be needed after that

if __name__ == '__main__' and __package__ is None:
    import_parents(level=3) # N = 3

from .. import settings

class TaskTimeLogger:
    """Compute and print the computation time taken by a section of the code"""

    def __init__(self):
        print('START')
        print('')
        self.global_start_t = time.time()
        self.global_start_c = time.perf_counter()
        self.task_start_t  = None
        self.task_start_c  = None
        self.current_task_name = None

    def end(self):
        """Final end : display final info"""
        print('END')
        print(f"time (t): {time.time() - self.global_start_t}")
        print(f"time (c): {time.perf_counter() - self.global_start_c}")
        print('')

    def start(self, task_name):
        """Start a new task & display info"""
        self.task_start_t = time.time()
        self.task_start_c = time.perf_counter()
        self.current_task_name = task_name
        print(f'STARTING {task_name}')

    def end_current_task(self):
        """End the current task & display info"""
        task_end_t = time.time()
        task_end_c = time.perf_counter()
        print(f'{self.current_task_name} END')
        print(f"time (t): {task_end_t - self.task_start_t}")
        print(f"time (c)): {task_end_c - self.task_start_c}")
        print('')


class CyclabDB:
    """Code for dealing with a cyclab db"""

    def __init__(self, log):
        """p is the parameters"""
        self.log = log
        self.conn = None

    def connect(self):
        """TBD"""
        self.log.start('DB CONNECTION')
        if settings.DB_PWD:
            self.conn = psycopg2.connect(
                database=settings.DB_NAME, user=settings.DB_USER,
                password=settings.DB_PWD, host=settings.DB_HOST,
                port=settings.DB_PORT)
        else:
            self.conn = psycopg2.connect(
                database=settings.DB_NAME, user=settings.DB_USER,
                host=settings.DB_HOST, port=settings.DB_PORT)
        self.log.end_current_task()
        self.cur = self.conn.cursor()
        return self.cur

    def new_cursor(self):
        """TBD"""
        return self.conn.cursor()

    def disconnect(self):
        """TBD"""
        self.cur.close()
        self.conn.commit()
        self.conn.close()


def save_and_delete_segments_and_their_votes(cur, log, deleted_segment_id):
    """Save (in a database called 'deleted_*') the segments having in
    delete, their associated intersection_paths and the votes for the deleted
    entities.

    Keyword arguments:
    cur -- a postgresql cursor
    log -- a logger
    deleted_segment_id -- a list of the id of the segments to delete
    """

    # pas besoin de partir des deleteds_nodes car fait a partir des segments
    if deleted_segment_id:
        log.start('COMPUTE DELETED INTERSECTION PATH')

        sql = f"SELECT id FROM {settings.TABLE_NAME['intersection_paths']} \
            WHERE segmentstart_id = any(ARRAY" + str(deleted_segment_id) + ") \
            OR segmentend_id = any(ARRAY" + str(deleted_segment_id) + ")   ;"
        cur.execute(sql)
        deleted_intersection_path_id = [int(row[0]) for row in cur]
        log.end_current_task()

        save_and_delete_interpath_and_their_votes(
            cur, log, deleted_intersection_path_id)

    log.start('SAVE AND DELETE SEGMENTS AND THEIR VOTES')
    if deleted_segment_id:
        cur.execute(f"""
INSERT INTO
    {settings.TABLE_NAME['deleted_segment_votes']}(
        id, vote_value, vote_creation_time, direction, profile_id, segment_id,
        user_id, vote_reason_id, latest)
SELECT
    id, vote_value, vote_creation_time, direction, profile_id, segment_id,
    user_id, vote_reason_id, latest
    FROM {settings.TABLE_NAME['segment_votes']}
WHERE segment_id = any(ARRAY {str(deleted_segment_id)}); """)

        cur.execute(f"DELETE FROM {settings.TABLE_NAME['segment_votes']} \
            WHERE segment_id = any(ARRAY{str(deleted_segment_id)});")

        cur.execute(f"""
INSERT INTO
    {settings.TABLE_NAME['deleted_segments']}(
    id, way_id, nodestart_id, nodeend_id, geom, vote_default, vote_average, vote_number,
    averagedata, vote_average_both, vote_average_ba, vote_average_ab
    )
SELECT
    id, way_id, nodestart_id, nodeend_id, geom, vote_default, vote_average, vote_number,
    averagedata, vote_average_both, vote_average_ba, vote_average_ab
FROM {settings.TABLE_NAME['segments']}
WHERE id = any(ARRAY {str(deleted_segment_id)});""")

        cur.execute(f"DELETE FROM {settings.TABLE_NAME['segments']} \
            WHERE id = any(ARRAY{str(deleted_segment_id)});")
    log.end_current_task()


def save_and_delete_interpath_and_their_votes(cur, log, deleted_intersection_path_id):
    """Save (in a database called 'deleted_*') the intersection_paths and
    their associated votes and delete them for the database.

    Keyword arguments:
    cur -- the postgresql cursor
    log -- the logger
    deleted_intersection_path_id -- a list of the intersections_paths id to delete
    """
    log.start('SAVE AND DELETE INTERSECTION PATHS AND THEIR VOTES')
    if deleted_intersection_path_id:
        cur.execute(f"""
INSERT INTO {settings.TABLE_NAME['deleted_intersection_path_votes']}  (
    id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, client, localisation, details, is_expert_vote, intersection_path_id)
SELECT
    id, vote_value, user_id, profile_id, datetime_vote, datetime_creation, client, localisation, details, is_expert_vote, intersection_path_id
FROM {settings.TABLE_NAME['intersection_path_votes']}
WHERE intersection_path_id = any(ARRAY {str(deleted_intersection_path_id) });""");
        cur.execute(f"DELETE FROM {settings.TABLE_NAME['intersection_path_votes']} \
            WHERE intersection_path_id = any(ARRAY { str(deleted_intersection_path_id) });")

        cur.execute(f"INSERT INTO {settings.TABLE_NAME['deleted_intersection_paths']} \
           SELECT * FROM {settings.TABLE_NAME['intersection_paths']} WHERE id = any(ARRAY" + str(deleted_intersection_path_id) + ");")
        cur.execute(f"DELETE FROM {settings.TABLE_NAME['intersection_paths']} \
           WHERE id = any(ARRAY" + str(deleted_intersection_path_id) + ");")
    log.end_current_task()


def save_osmosis_deleted_entities(cur, log, deleted_nodes, deleted_ways):
    """Save all the entities that will be deleted by osmosis : the nodes,
    the ways, the associated intersection_paths, the associated segments and
    their associated votes

    For the associated elements, the elements are deleted form their original
    table. For the nodes and ways, osmosis will do it

    Keyword arguments:
    cur -- the postgres cursor
    deleted_nodes -- an array of deleted nodes id
    deleted_ways -- an array of the deleted ways id
    """
    log.start('COMPUTE DELETED SEGMENTS')
    sql = f"SELECT id FROM {settings.TABLE_NAME['segments']} WHERE way_id = any(ARRAY {str(deleted_ways)} );"
    cur.execute(sql)
    deleted_segment_id = [int(row[0]) for row in cur]
    log.end_current_task()

    log.start('COPY DELETED NODES')
    sql = f"INSERT INTO {settings.TABLE_NAME['deleted_nodes']} SELECT * FROM nodes WHERE id = any(ARRAY {str(deleted_nodes)} );"
    cur.execute(sql)
    log.end_current_task()

    log.start('COPY DELETED WAYS')
    # """ SINON VONT ETRE SUPPRIMER PAR OSMOSIS """
    sql = f"INSERT INTO {settings.TABLE_NAME['deleted_ways']} SELECT * FROM {settings.TABLE_NAME['ways']} WHERE id = any(ARRAY {str(deleted_ways)} );"
    cur.execute(sql)
    log.end_current_task()

    save_and_delete_segments_and_their_votes(cur, log, deleted_segment_id)


def exe_osmosis_update(log):
    """Start the osmosis update procedure

    Keyword arguments:
    - log -- osmosis updating
    """
    log.start('OSMOSIS UPDATING')
    subprocess.call([
        osmosis_exec, "--read-xml-change", tmp_osc_filename,
        "--write-pgsql-change", f"host={settings.DB_HOST}:{settings.DB_PORT}",
        f"database={settings.DB_NAME}", f"user={settings.DB_USER}",
        f"password={settings.DB_PWD}"])
    log.end_current_task()


def update_segment(
    cur, way_id, nodestart_id, nodeend_id, nodes_point_list,
    db_current_segments
        ):
    """ Update a segment :
        - update the geometry if the segment still exist after the osmosis update
            ( it is the case if a segment in db_current_segments with the
            same nodestart_id and nodeend_id is founded ;
            when a segment is updated, it is removed form db_current_segments )
        - add the segment if it do not exist after the osmosis update

    Keyword arguments:
    cur -- The postgresql cursor
    way_id --  Id of the way containing the segment (after the update)
    nodestart_id -- Id of the fist node of the segment (after the update)
    nodeend_id --  Id of the last node of the segment (after the update)
    nodes_point_list -- List of the node of the segment (after the update)
    db_current_segments -- list of the segments that are in the db (to be updated)

    at the end db_current_segments contains the list of the segment that were
    not updated (to remove)
    """
    segment_position = -1
    for i in range(0, len(db_current_segments)):
        if (db_current_segments[i][1] == nodestart_id and db_current_segments[i][2] == nodeend_id) or \
            (db_current_segments[i][1] == nodeend_id and db_current_segments[i][2] == nodestart_id) :
            segment_position = i
            break
    if segment_position >= 0:
        db_current_segments.pop(i)
        cur.execute(
            f"UPDATE {settings.TABLE_NAME['segments']} SET geom = %s WHERE id = %s" %
            (db_fct.sql_line(nodes_point_list), way_id))
    else:
        db_fct.db_add_segment(
            cur, way_id, nodestart_id, nodeend_id, current_segment)


# """
# - problème : déplacer tous les votes dans une colonne deleted
# """

log = TaskTimeLogger()
db = CyclabDB(log)

tmp_pbf_filename = "TMP.pbf"
tmp_osc_filename = "TMP-diff.osc"

if len(sys.argv) <= 1:
    print("usage : python update.py path_to_osm_pbf_file [path to osmosis]")
    sys.exit()

osm_pbf_input = sys.argv[1]

if len(sys.argv) >= 3:
    osmosis_exec = sys.argv[2]
else:
    osmosis_exec = "osmosis"


if __name__ == '__main__':

    cur  = db.connect()
    cur.execute("""SELECT EXISTS (
   SELECT FROM information_schema.tables 
   WHERE  table_name   = 'replication_changes'
   );
        """)
    has_rep_changes_table = cur.fetchone()[0]
    print(has_rep_changes_table)
    if not has_rep_changes_table:
        print('Relation "replication_changes" does not exist')
        print('Please create if using the file /opt/osmosis/script/pgsnapshot_schema_0.6_changes.sql')
        raise Exception()

    log.start('OSM FILE CREATION')
    print('CREATION DE ' + tmp_osc_filename + ' non faite\n')

    print('---------')
    print(f"{osmosis_exec} --read-pgsql database={settings.DB_NAME} \
        user={settings.DB_USER} password={settings.DB_PWD} \
        host={settings.DB_HOST}:{settings.DB_PORT}" + " outPipe.0=pg" + " --dd" + \
        " inPipe.0=pg" + " outPipe.0=dd" + " --write-pbf" + " inPipe.0=dd" + \
        f" file={tmp_pbf_filename}")
    print('--------')

    subprocess.call([
        osmosis_exec, "--read-pgsql", "database={}".format(settings.DB_NAME),
        "user={}".format(settings.DB_USER), "password={}".format(settings.DB_PWD),
        "host={}:{}".format(settings.DB_HOST, settings.DB_PORT), "outPipe.0=pg", "--dd",
        "inPipe.0=pg", "outPipe.0=dd", "--write-pbf", "inPipe.0=dd",
        "file={}".format(tmp_pbf_filename)])

    subprocess.call([
        osmosis_exec, "--read-pbf", osm_pbf_input, "--tf", "accept-ways",
        "highway=*", "--tf", "reject-relations", "--used-node",
        "--read-pbf", tmp_pbf_filename, "--derive-change", "--write-xml-change",
        "file={}".format(tmp_osc_filename)])

    log.end_current_task()

    log.start('BUFFERING TMP OSC FILE')
    nodes = {}
    nodes['delete'] = []
    nodes['modify'] = []
    nodes['create'] = []
    ways = {}
    ways['delete'] = []
    ways['modify'] = []
    ways['create'] = []

    action = ''

    for (event, elem) in ElementTree.iterparse(tmp_osc_filename, events=['start']):
        if elem.tag == 'delete' or elem.tag == 'modify' or elem.tag == 'create':
            action = elem.tag
        else:
            if elem.tag == 'node':
                nodes[action].append(int(elem.attrib['id']))
            elif elem.tag == 'way':
                ways[action].append(int(elem.attrib['id']))

    print(len(nodes['delete']))
    print(len(nodes['modify']))
    print(len(nodes['create']))
    print(len(ways['delete']))
    print(len(ways['modify']))
    print(len(ways['create']))
    log.end_current_task()

    cur = db.connect()

    save_osmosis_deleted_entities(cur, log, nodes['delete'], ways['delete'])

    db.disconnect()

    exe_osmosis_update(log)

    cur = db.connect()


    """ Recalculer le nombre de chemin qui passe par un noeud"""

    log.start('TMP NODES TABLE CREATION')
    # doit se faire sur tous les noeuds

    # TO DELETE
    # nodes_to_process = nodes['modify'][:]  # copy de nodes modify
    # nodes_to_process.extend(nodes['create'])  # mis dans un meme tableau car meme procedure
    #
    # print(len(nodes_to_process))

    cur.execute(f"CREATE TEMPORARY TABLE tmp_nodes (\
       id BIGINT NOT NULL, \
       ways_nbr BIGINT NOT NULL, \
       PRIMARY KEY(id)); \
       \
       INSERT INTO tmp_nodes (id, ways_nbr) \
       SELECT n.id AS id, COUNT(w.id) AS ways_nbr \
          FROM {settings.TABLE_NAME['nodes']} AS n \
          JOIN {settings.TABLE_NAME['ways']} as w ON n.id = ANY(w.nodes) \
          GROUP BY n.id \
          ")
    #     HAVING  COUNT(w.id) > 1;"
    #             )
    log.end_current_task()


    log.start('UPDATING NODES TABLE')
    cur.execute("UPDATE nodes AS n \
       SET ways_nbr = tmp.ways_nbr \
       FROM tmp_nodes as tmp WHERE n.id = tmp.id")
       # AND tmp.ways_nbr > 1 "sinon si ajout d'un way sur un point, pas de détection"
       # AND n.id  = any(ARRAY" + str(nodes_to_process) + ")")
    log.end_current_task()


    log.start('BUFFERING NODES')
    cur.execute(f"SELECT id, geom, ways_nbr FROM {settings.TABLE_NAME['nodes']}")
    buff_nodes = {}

    for row in cur:
        buff_nodes[row[0]] = (row[1], row[2])
    log.end_current_task()


    # """ Debug """

    log.start('UPDATING SEGMENTS - CREATE')
    # """POUR LES WAYS CREES -> AJOUT DES NOUVEAUX SEGMENTS"""
    # """LES INTERSECTION ONT ETES RAJOUTES"""
    # """AVOIR LE MEME CODE QU'IMPORT?"""
    # """ EN FAIT IMPORT = UPDATE MAIS AVEC RIEN AU DEBUT"""
    cur.execute(f"SELECT id, nodes, tags FROM {settings.TABLE_NAME['ways']} WHERE id = any(ARRAY[" + str(ways['create']) + "])")
    cur2 = db.new_cursor() # comment to debug

    for row in cur:
        way_id = row[0]
        way_nodes = row[1]
        way_tags = row[2]
        current_segment = [buff_nodes[way_nodes[0]][0]]
        nodestart_id = way_nodes[0]
        for n in way_nodes[1:]:
            current_segment.append(buff_nodes[n][0])
            nodeend_id = n
            if buff_nodes[n][1] > 1:
                db_fct.db_add_segment( # comment to debug
                    # print('ADD NEW SEGMENT w_id: {}, nodestart_id: {}, nodeend_id: {} current semgnet: {}'.format(
                    cur2, # comment to debug
                    way_id, nodestart_id, nodeend_id, current_segment)
                    #)
                current_segment = [buff_nodes[n][0]]
                nodestart_id = n
        if len(current_segment) > 1:
            db_fct.db_add_segment(  # comment to debug
                # print('ADD NEW SEGMENT w_id: {}, nodestart_id: {}, nodeend_id: {} current semgnet: {}'.format(
                cur2,  # comment to debug
                way_id, nodestart_id, nodeend_id, current_segment)
                #)
    cur2.close()  # comment to debug
    log.end_current_task()

    # """ debug
    print("MAJ SEGMENTS NODES TABLE")
    print("TODO MODIFY")
    # fait en fct du way : si supprimer -> on supprime le segment
    # si ajouter on ajouter le segment
    # si modifier on repasse sur les segments


    log.start("UPDATING SEGMENTS - MODIFY")
    cur.execute(
        f"SELECT id, nodes, tags FROM {settings.TABLE_NAME['ways']} WHERE id = any(ARRAY[" +
        str(ways['modify']) + "])")
    cur2 = db.new_cursor()

    for row in cur:
        way_id = row[0]
        way_nodes = row[1]
        way_tags = row[2]

        db_current_segments = []
        cur2.execute(
            f"SELECT id, nodestart_id, nodeend_id FROM {settings.TABLE_NAME['segments']} WHERE way_id = " + str(way_id))
        for row in cur2:
            db_current_segments.append(row)

        current_segment = [buff_nodes[way_nodes[0]][0]]
        nodestart_id = way_nodes[0]
        for n in way_nodes[1:]:
            current_segment.append(buff_nodes[n][0])
            nodeend_id = n
            if buff_nodes[n][1] > 1:  # debut d'un segment
                update_segment(
                    cur2, way_id, nodestart_id, nodeend_id, current_segment,
                    db_current_segments)
                current_segment = [buff_nodes[n][0]]
                nodestart_id = n
        if len(current_segment) > 1:
            update_segment(
                cur2, way_id, nodestart_id, nodeend_id, current_segment,
                db_current_segments)

        segment_id_to_del = [int(s[0]) for s in db_current_segments]
        if segment_id_to_del:
            save_and_delete_segments_and_their_votes(cur2, log, segment_id_to_del)
    log.end_current_task()

    cur2.close()
    # """

    db.disconnect()

    # subprocess.call(["rm", tmp_osc_filename])

    log.end()
