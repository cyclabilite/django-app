import time

class TaskTimeLogger:
    """Compute and print the computation time taken by a section of the code"""

    def __init__(self):
        print('START')
        print('')
        self.global_start_t = time.time()
        self.global_start_c = time.perf_counter()
        self.task_start_t  = None
        self.task_start_c  = None
        self.current_task_name = None

    def end(self):
        """Final end : display final info"""
        print('END')
        print(f"time (t): {time.time() - self.global_start_t}")
        print(f"time (c): {time.perf_counter() - self.global_start_c}")
        print('')

    def start(self, task_name):
        """Start a new task & display info"""
        self.task_start_t = time.time()
        self.task_start_c = time.perf_counter()
        self.current_task_name = task_name
        print(f'STARTING {task_name}')

    def end_current_task(self):
        """End the current task & display info"""
        task_end_t = time.time()
        task_end_c = time.perf_counter()
        print(f'{self.current_task_name} END')
        print(f"time (t): {task_end_t - self.task_start_t}")
        print(f"time (c)): {task_end_c - self.task_start_c}")
        print('')
