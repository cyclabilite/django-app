ALTER TABLE ways ADD COLUMN vote_default int DEFAULT 3 NULL;
ALTER TABLE ways ADD COLUMN vote_number int DEFAULT 0 NOT NULL;
ALTER TABLE ways ADD COLUMN vote_average float8 NULL;

ALTER TABLE nodes ADD COLUMN ways_nbr int DEFAULT 0;