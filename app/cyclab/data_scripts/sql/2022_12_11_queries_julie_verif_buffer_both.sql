-- version qui cherche si combinaison segments des 2 contes

-- ici buffer :
--  new_buffer_size = 10
-- del_buffer_size = 10
-- a changer si besoin


--  # Step 1 : Creation cyclab_segments_l93 (en option)
DROP TABLE IF EXISTS cyclab_segments_l93;

CREATE TABLE cyclab_segments_l93 AS (
SELECT
    s.*,
    ST_Transform(s.geom, 2154) as geom_l93,
    ST_Buffer(ST_Transform(s.geom, 2154), 10 /* new_buffer_size*/ ) AS buff_l93
FROM cyclab_segments as s);


-- # Step 2 : Creation index pour cyclab_segments_l93 (en option)
CREATE INDEX ON "cyclab_segments_l93" USING GIST("geom_l93");
CREATE INDEX ON "cyclab_segments_l93" ("id");
CREATE INDEX ON "cyclab_segments_l93" USING GIST("buff_l93");


-- # Step 3 :Creation deleted_cyclab_segments_l93 & index (en option)
DROP TABLE IF EXISTS deleted_cyclab_segments_l93;
CREATE TABLE deleted_cyclab_segments_l93 AS (
	SELECT
    s.id AS id,
    ST_Transform(s.geom, 2154) as geom_l93,
    ST_Buffer(ST_Transform(s.geom, 2154), 10 /*del_buffer_size*/) AS buff_l93
    , count(*) AS vote_nbr
FROM deleted_cyclab_segments as s
JOIN cyclab_deletedvotesegment as v ON v.segment_id = s.id
GROUP BY s.id, s.geom
HAVING COUNT(*) > 0);

CREATE INDEX ON "deleted_cyclab_segments_l93" USING  GIST("geom_l93");
CREATE INDEX ON "deleted_cyclab_segments_l93" ("id");
CREATE INDEX ON "deleted_cyclab_segments_l93" USING GIST("buff_l93");
            

-- Deleted segment dans buffer de new segments            
DROP TABLE IF EXISTS del_segments_in_new_segment_buffer;

CREATE TABLE del_segments_in_new_segment_buffer AS (
    SELECT del_s.id del_seg_id, s.id as new_seg_id__buff
    FROM cyclab_segments_l93 as s
    JOIN deleted_cyclab_segments_l93 as del_s ON ST_Covers(del_s.buff_l93, s.geom_l93));


-- New segment dans buffer de deleted buffer     
DROP TABLE IF EXISTS new_segments_in_del_segment_buffer;

CREATE TABLE new_segments_in_del_segment_buffer AS (
    SELECT s.id new_seg_id, del_s.id as del_seg_id__buff
    FROM cyclab_segments_l93 as s
    JOIN deleted_cyclab_segments_l93 as del_s ON ST_Covers(s.buff_l93, del_s.geom_l93));


DROP TABLE IF EXISTS del_segments_having_one_prop_to_trans;
CREATE TABLE del_segments_having_one_prop_to_trans AS (
    SELECT dib.del_seg_id as del_seg_id, COUNT(*)
    FROM del_segments_in_new_segment_buffer as dib
    JOIN new_segments_in_del_segment_buffer as nib
        ON  dib.del_seg_id = nib.del_seg_id__buff
        AND dib.new_seg_id__buff = nib.new_seg_id
        GROUP BY dib.del_seg_id
        HAVING COUNT(*) = 1);

DROP TABLE IF EXISTS prop_transfer;

CREATE TABLE prop_transfer AS (
    SELECT 
        dib.del_seg_id as del_seg_id,
        nib.new_seg_id as new_seg_id
    FROM del_segments_in_new_segment_buffer as dib
    JOIN new_segments_in_del_segment_buffer as nib
        ON  dib.del_seg_id = nib.del_seg_id__buff
        AND dib.new_seg_id__buff = nib.new_seg_id
        AND dib.del_seg_id IN (SELECT del_seg_id FROM del_segments_having_one_prop_to_trans)
);

SELECT COUNT(*) FROM public.prop_transfer
