import psycopg2
from pathlib import Path

class CyclabDB:
    """Code for dealing with a cyclab db"""

    def __init__(self, settings, log):
        """p is the parameters"""
        self.log = log
        self.settings = settings
        self.conn = None
        self.curs = []

    def connect(self):
        """TBD"""
        self.log.start('DB CONNECTION')
        if self.settings.DB_PWD:
            self.conn = psycopg2.connect(
                database=self.settings.DB_NAME, user=self.settings.DB_USER,
                password=self.settings.DB_PWD, host=self.settings.DB_HOST,
                port=self.settings.DB_PORT)
        else:
            self.conn = psycopg2.connect(
                database=self.settings.DB_NAME, user=self.settings.DB_USER,
                host=self.settings.DB_HOST, port=self.settings.DB_PORT)
        self.log.end_current_task()
        self.curs.append(self.conn.cursor())
        return self.curs[0]

    def new_cursor(self):
        """TBD"""
        new_cur = self.conn.cursor()
        self.curs.append(new_cur)
        return new_cur

    def commit(self):
        self.conn.commit()

    def disconnect(self):
        """TBD"""
        for c in self.curs:
            c.close()
        self.conn.commit()
        self.conn.close()

    def exec_file(self, filename):
        f = Path(filename)
        self.curs[0].execute(f.open().read())
