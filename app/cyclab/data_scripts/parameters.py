import os

DB_USER = 'postgres'
DB_HOST = 'db'
DB_PORT = '5432'
DB_PWD = 'postgres'
DB_NAME = 'postgres'

TABLE_NAME = {
    'segments': 'cyclab_segments', # table name for segments
    'segment_votes': 'cyclab_votesegment', # table name for votes on segments
    'deleted_segments': 'deleted_cyclab_segments',  # on deleted segments
    'deleted_segment_votes': 'cyclab_deletedvotesegment',  # delted votes on segments
    'deleted_nodes': 'deleted_nodes',
    'deleted_ways': 'deleted_ways',
    'nodes': 'nodes',
    'ways': 'ways',
    # TODO : remove the following tables (?)
    'intersection_paths': 'cyclab_intersection_paths',
    'deleted_intersection_paths': 'deleted_cyclab_intersection_paths',
    'deleted_intersection_path_votes': 'deleted_cyclab_intersection_path_vote',
    'intersection_path_votes': 'cyclab_intersection_path_vote',
}

if 'DB_HOST' in os.environ:
    DB_HOST = os.environ['DB_HOST']

print(f'pg user: {DB_USER}')
print(f'pg host: {DB_HOST}')
print(f'pg pwd : {DB_PWD}')
print(f'pg name: {DB_NAME}')
