"""
Django settings for cyclab project.

Generated by 'django-admin startproject' using Django 3.1.7.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

from pathlib import Path
import os
from django.utils.translation import gettext_lazy as _
# import environ


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Initialise environment variables
# env=environ.Env()
# environ.Env.read_env()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG_ARG = os.getenv('DEBUG')

DEBUG = False

if DEBUG_ARG and DEBUG_ARG == '1':
    DEBUG = True

ALLOWED_HOSTS_PARAM = os.getenv('ALLOWED_HOST')

ALLOWED_HOSTS = []

if ALLOWED_HOSTS_PARAM:
    ALLOWED_HOSTS = ALLOWED_HOSTS_PARAM.split(',')

# Application definition

INSTALLED_APPS = [
    'cyclab.apps.CyclabConfig',
    'django.contrib.gis',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.auth',
    'leaflet',
    'rest_framework',
    'rest_framework_gis',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'cyclab.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'cyclab/templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'cyclab.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.getenv('DB_NAME', 'postgres'),
        'USER': os.getenv('DB_USER', 'postgres'),
        'PASSWORD': os.getenv('DB_PWD', 'postgres'),
        'HOST': os.getenv('DB_HOST', 'db'),
        'PORT': os.getenv('DB_PORT', '5432'),
    },
}


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'fr'

LANGUAGES = [
  ('fr', _('Français')),
]

TIME_ZONE = 'Europe/Brussels'

DATE_FORMAT = "d-m-Y"

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = [
    os.path.join(Path(__file__).resolve().parent, "locale"),
]


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'

STATIC_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]

STATIC_ROOT = '/app/cyclab/static'

LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (50.9007, 4.4888),
    'DEFAULT_ZOOM': 5,
    'MAX_ZOOM': 20,
    'MIN_ZOOM': 3,
    'SCALE': 'both',
    'ATTRIBUTION_PREFIX': 'Inspired by Life in GIS'
}

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

LOGIN_REDIRECT_URL = 'index'
LOGOUT_REDIRECT_URL = 'index'

AUTH_USER_MODEL = 'cyclab.User'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

EMAIL_SMTP = os.getenv('EMAIL_SMTP')

if EMAIL_SMTP and EMAIL_SMTP == '1':
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = os.getenv('EMAIL_HOST', 'localhost')
    # EMAIL_PORT = os.getenv('EMAIL_PORT', 1025)
    EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER', '')
    EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD', '')
    # EMAIL_USE_TLS = os.getenv('EMAIL_USE_TLS', False)

DEFAULT_FROM_EMAIL = os.getenv('DEFAULT_FROM_EMAIL')

MEDIA_ROOT = os.path.join(BASE_DIR, 'cyclab/media')

SITE_URL = os.getenv('SITE_URL', 'http://localhost:8000')

# For data scripts & commands
DB_USER = DATABASES['default']['USER']
DB_HOST = DATABASES['default']['HOST']
DB_PORT = DATABASES['default']['PORT']
DB_PWD = DATABASES['default']['PASSWORD']
DB_NAME = DATABASES['default']['NAME']

TABLE_NAME = {
    'segments': 'cyclab_segments', # table name for segments
    'segment_votes': 'cyclab_votesegment', # table name for votes on segments
    'deleted_segments': 'deleted_cyclab_segments',  # on deleted segments
    'deleted_segment_votes': 'cyclab_deletedvotesegment',  # delted votes on segments
    'deleted_nodes': 'deleted_nodes',
    'deleted_ways': 'deleted_ways',
    'nodes': 'nodes',
    'ways': 'ways',
    # TODO : remove the following tables (?)
    'intersection_paths': 'cyclab_intersection_paths',
    'deleted_intersection_paths': 'deleted_cyclab_intersection_paths',
    'deleted_intersection_path_votes': 'deleted_cyclab_intersection_path_vote',
    'intersection_path_votes': 'cyclab_intersection_path_vote',
}