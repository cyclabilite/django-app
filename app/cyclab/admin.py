from datetime import datetime
from leaflet.admin import LeafletGeoAdmin

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.safestring import mark_safe

from .models import City, User, VoteReason, SegmentsDeleted, SegmentsDeletedToHandle
from .forms import RegisterForm

class CyblabUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User

class CyclabUserAdmin(UserAdmin):
    form = CyblabUserChangeForm
    add_form = RegisterForm
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('gender', 'birth_year', 'is_email_verified', 'is_expert')}),
    )

    list_display = (
        'id', 'email', 'first_name', 'last_name', 'is_staff', 'is_expert', 'is_email_verified', 'is_active')

class DeletedSegmentsAdmin(admin.ModelAdmin):
    list_display = ('id', 'way_id', 'nodestart_id', 'nodeend_id', 'vote_default', 'vote_average', 'vote_number', 'infrigo', 'frigo_datetime')
    fieldsets = (None, {'fields': ('infrigo',)}),
    list_filter = ('infrigo',)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        try:
            extra_context['transfer_vote'] = True
        except TypeError:
            extra_context = {'transfer_vote': True}
        return self.changeform_view(request, object_id, form_url, extra_context)

@receiver(pre_save, sender=SegmentsDeleted)
def my_handler_deleted_segment(sender, instance, **kwargs):
    sd = SegmentsDeleted.objects.get(id=instance.id)
    if sd.infrigo == False and instance.infrigo == True:
        try:
            instance.frigo_is_manuel = True
            instance.frigo_datetime = datetime.now()
        except SegmentsDeleted.DoesNotExist:
            pass

class DeletedSegmentsToHandleAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'way_id', 'nodestart_id', 'nodeend_id', 'vote_default', 'vote_average',
        'vote_number', 'transfer_action', 'infrigo_action')

    #show only deleted segments that have at least one vote
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(vote_number__gt=0, infrigo=False)

    def transfer_action(self, obj):
        return mark_safe(f'<a href="/admin/vote-transfer/{obj.id}">Transfert manuel</a>')

    def infrigo_action(self, obj):
        return mark_safe(f'<a href="/admin/set-in-frigo/{obj.id}">Mettre au frigo</a>')

class CityAdmin(LeafletGeoAdmin):
    list_display = ('city_name', 'is_default')
    # Listen to a city being saved as default in admin


@receiver(pre_save, sender=City)
def my_handler_default_city(sender, instance, **kwargs):
    if instance.is_default == True:
        try:
            d = City.objects.get(is_default=True)
            d.change_default()
            d.save()
        except City.DoesNotExist:
            pass

# Register your models here.
admin.site.register(City, CityAdmin)
admin.site.register(User, CyclabUserAdmin)
admin.site.register(VoteReason)
admin.site.register(SegmentsDeleted, DeletedSegmentsAdmin)
admin.site.register(SegmentsDeletedToHandle, DeletedSegmentsToHandleAdmin)
