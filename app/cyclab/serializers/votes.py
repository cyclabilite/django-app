from rest_framework import serializers
from ..models.votes import VoteSegment, VoteReason
from .nested import SimpleSegmentSerializer
from .user import ProfileSerializer

class VoteReasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = VoteReason
        fields = '__all__'

class VoteSerializer(serializers.ModelSerializer):
    segment_object = SimpleSegmentSerializer(read_only=True)
    username = serializers.CharField(max_length=50, read_only=True)
    reason_text = serializers.CharField(max_length=200, read_only=True)

    class Meta:
        model = VoteSegment
        fields = [
            'id', 'segment', 'direction', 'vote_reason', 'reason_text',
            'vote_value', 'vote_creation_time', 'user', 'username',
            'segment_object', 'latest']