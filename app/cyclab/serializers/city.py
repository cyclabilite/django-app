from rest_framework import serializers
from ..models.city import City

class CitySerializer(serializers.ModelSerializer):
    lat = serializers.FloatField(read_only=True)
    lon = serializers.FloatField(read_only=True)

    class Meta:
        model = City
        fields = ['city_name', 'point',
                'zoom_level', 'is_default', 'lon', 'lat']
        read_only_fields = ('lon', 'lat')
