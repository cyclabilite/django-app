from rest_framework_gis.serializers import GeoFeatureModelSerializer
from ..models.osm import Segments, Nodes

class NodesSerializer(GeoFeatureModelSerializer):

    class Meta:
        model = Nodes
        geo_field = 'geom'
        fields = []

class SimpleSegmentSerializer(GeoFeatureModelSerializer):
    nodestart = NodesSerializer(read_only=True)
    nodeend = NodesSerializer(read_only=True)

    class Meta:
        model = Segments
        geo_field = 'geom'
        fields = ['id', 'nodestart', 'nodeend']