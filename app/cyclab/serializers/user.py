from rest_framework import serializers
from ..models.user import UserProfile, User

class ProfileShortSerializer(serializers.ModelSerializer):
    label = serializers.CharField(max_length=50, read_only=True)
    class Meta:
        model= UserProfile
        fields= '__all__'

class UserSerializer(serializers.ModelSerializer):

    profiles = ProfileShortSerializer(many=True)

    class Meta:
        model = User
        fields = ['id', 'profiles', 'username', 'first_name', 'last_name',
                'birth_year', 'is_expert', 'gender', 'date_joined']

class ProfileSerializer(serializers.ModelSerializer):

    label = serializers.CharField(max_length=50, read_only=True)
    user = UserSerializer(read_only=True)

    class Meta:
        model = UserProfile
        fields = ['id', 'user', 'label',
                'rider_type', 'rapidity']
