from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from ..models.osm import Segments, Ways, SegmentsDeleted
from .votes import VoteSerializer
from .nested import SimpleSegmentSerializer

class WaysSerializer(GeoFeatureModelSerializer):
    segments = SimpleSegmentSerializer(many=True, read_only=True)

    class Meta:
        model = Ways
        geo_field = 'linestring'
        fields = [
            'id', 'segments', 'version', 'user_id', 'tstamp', 'changeset_id', 'tags', 'nodes',
            'linestring']

class SegmentsSerializer(serializers.ModelSerializer):

    type = serializers.CharField(default='segment', read_only=True)
    vote_number = serializers.IntegerField()
    vote_average = serializers.FloatField()
    votes = serializers.SerializerMethodField('get_votes')

    way = WaysSerializer(read_only=True)

    def get_votes(self, obj):
        queryset = obj.votes.filter(latest=True)
        serializer = VoteSerializer(queryset, many=True, read_only=True)
        return serializer.data

    class Meta:
        model = Segments
        fields = [
            'id', 'way', 'type',
            'nodestart', 'nodeend', 'geom', 'vote_default', 'vote_number',
            'vote_average', 'votes']

class SegmentsDeletedSerializer(serializers.ModelSerializer):

    type = serializers.CharField(default='segment', read_only=True)
    vote_number = serializers.IntegerField()
    vote_average = serializers.FloatField()
    votes = VoteSerializer(many=True, read_only=True)
    way = WaysSerializer(read_only=True)

    class Meta:
        model = SegmentsDeleted
        fields = [
            'id', 'way', 'type', 'nodestart', 'nodeend', 'geom',
            'vote_default', 'vote_number', 'vote_average',
            'votes', 'infrigo', 'frigo_is_manuel', 'frigo_datetime']
