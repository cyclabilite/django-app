from django.apps import AppConfig


class CyclabConfig(AppConfig):
    name = 'cyclab'
