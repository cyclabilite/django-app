FROM ubuntu:20.04 AS osmosisbuilder
# GET osmosis exe file

## this Dockerfile is derived from original osmose Dockerfile:
## https://github.com/osm-fr/osmose-backend/blob/223bc167f3f2b7706fad3c6faa84fe3701d03af0/docker/Dockerfile
## 
## The following addition are made:
## * remove the osm_pbf_parser part
## * adding osmosis
## * adding osmium

ENV OSMOSIS_VERSION="0.48.3"

# add poly file argument, an URL to a POLY file. This filter will be 
# applyied to osc files during updates
# ARG POLYFILE

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
      wget \
      zip && \
    rm -rf /var/lib/apt/lists/*

ADD https://github.com/openstreetmap/osmosis/releases/download/$OSMOSIS_VERSION/osmosis-$OSMOSIS_VERSION.tgz /tmp/osmosis/

RUN cd /tmp/osmosis && tar xvfz osmosis-$OSMOSIS_VERSION.tgz \
    && rm osmosis-$OSMOSIS_VERSION.tgz \
    && chmod a+x bin/osmosis


FROM python:3.9 
# The container with the app

ENV PYTHONUNBUFFERED=1

WORKDIR /app

RUN apt-get update && apt-get install -y \
    libgdal-dev && \
    rm -rf /var/lib/apt/lists/*

# OSMOSIS INSTALL 
# 1. install java
RUN apt-get update && apt-get install -y \
    openjdk-11-jre-headless && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y \
    gettext && \
    rm -rf /var/lib/apt/lists/*

# 2. import last version of osmosis
COPY --from=osmosisbuilder /tmp/osmosis /opt/osmosis
# add osmosis to path
ENV PATH="/opt/osmosis/bin:${PATH}"
# import poly file, used for clipping osc minutely files
# COPY --from=osmosisbuilder /etc/osmosis-update/filter.poly /etc/osmosis-update/filter.poly


RUN pip3 install --upgrade pip
RUN pip3 install setuptools==58
RUN pip3 install gdal==$(gdal-config --version)

COPY . /app

# create the directory where the export-csv file will be stored
RUN mkdir -p /app/cyclab/media
RUN mkdir -p /app/cyclab/media/export
RUN chmod a+rwx -R  /app/cyclab/media/export
RUN echo "# you should run the django command exportsegvotes for generating this file (i.e. python manage.py  exportsegvotes)" > "/app/cyclab/media/export/segments-notes.csv"

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt


EXPOSE 8000

CMD ["gunicorn", "-b", "0.0.0.0:8000", "cyclab.wsgi:application"]
