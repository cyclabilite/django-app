# Django version of cyclab

## Install

```
$ cp env.dist .env
$ mkdir data
$ mkdir -p app/cyclab/static/admin/
$ docker-compose up
```

## Add data into database (from osmosis), migrate & load features

```
$ docker exec -i  $(docker-compose ps -q db)  psql -U postgres postgres < lln-cyclab-db-dump.sql # You can find the db container into in the output of the command `docker ps`.
$ docker-compose exec app --user 1000 python3 manage.py migrate
$ docker-compose exec app --user 1000 python manage.py loaddata cyclab/features/cities.json
$ docker-compose exec app --user 1000 python manage.py loaddata cyclab/features/votereasons.json
```

## Create a super user

```
$ docker-compose exec app --user 1000 python manage.py createsuperuser
```

## Django commands

```
# to export vote in a csv file
$ docker-compose exec app --user 1000 python manage.py exportsegvotes  (+/- 5sec)

# Command to put all current deleted segments in the fridge and set a timestamp (+/- 1h )
$ docker-compose exec app --user 1000 python manage.py frigo

# Recompute the default vote (see `rules_to_apply.py` )
$ docker-compose exec app --user 1000 python manage.py setdefaultvote (+/- 1M )

# Recompute the average
$ docker-compose exec app --user 1000 python manage.py votecalc (+/- 15M )

# Recompute the attribute `latest` for each vote (better to do it after recompute the average)
$ docker-compose exec app --user 1000 python manage.py updlatest  (+/- 15M )

# Transfer votes by CLI in large numbers
$ docker-compose exec app --user 1000 python manage.py handle_trivial_transfer (pass necessary arguments bufferNew, bufferDelete, amount of segments)

```


- python manage.py exportsegvotes
-  python manage.py setdefaultvote (si rules to apply ok)
- login mathias

## Set/update default vote

Edit the file `rules_to_apply.py` then :

```
$ docker-compose exec app --user 1000 python manage.py setdefaultvote
```


## Update the road network

### Get the data for hauts-de-France

1. Download le that from geofabrik :

   - https://osm-internal.download.geofabrik.de/europe/france/nord-pas-de-calais-latest-internal.osm.pbf
   - https://osm-internal.download.geofabrik.de/europe/france/picardie-latest-internal.osm.pbf

2. Merge to a single osm.pbf file :


```
$ osmosis --read-pbf cyclab/data_scripts/nord-pas-de-calais-latest-internal.osm.pbf --read-pbf cyclab/data_scripts/picardie-latest-internal.osm.pbf --merge --write-pbf  cyclab/data_scripts/haut-de-france.osm.pbf
```

### Lancer la mise à jour

Put the deleted votes in the `frigo`.

Get the .osm.pbf data (see previous paragraph)

Go inside the the folder `cyclab/data_scripts` in an `app` container :

```
$ docker-compose exec app --user 1000 bash
app $ cd cyclab/data_scripts
```

Then run the script `update_after_new_import.py`

```
app $ python update_after_new_import.py path_to_osm_pbf_file
app $ # python update_after_new_import.py path_to_osm_pbf_file
```


## Commands

- list the containers :

```
$ docker-compose ls
```

- run the containers :

```
$ docker-compose up
```

- access to the container app (when running):

```
$ docker-compose exec app bash
```

## Kubernate hosting 

# to be sure that the style of the admin will be in the image
# if it fails, run in the app container : python manage.py collectstatic --noinput 
COPY ./cyclab/static/admin/ /usr/share/nginx/html/admin/


## Compile .js file

```
   $ docker-compose run --rm webpack bash
wp $ npm install
wp $ npx webpack
```

## Load a db dump

Go inside the `db` container :

```
docker-compose -f docker-compose.yml  -f docker-compose.dev.yml exec db bash     
```

Using the utilitary `psql` (as `postgres` user), drop and recreate the `public` schema :

```
root@5ecda08a9ff5:/# psql -U postgres
postgres=# DROP SCHEMA public CASCADE;
postgres=# CREATE SCHEMA public;
postgres=# DROP SCHEMA tiger, tiger_data, topology;
postgres=# CREATE ROLE cyclabprod;
```
Then load the dump

```
root@5ecda08a9ff5:/# psql -U postgres < /file_where_the_dump_is.sql
```

## Occurring errors

### `docker-compose up` fails on a error about `static/admin` directory does not exist

```
$ docker-composer run app  python manage.py collectstatic --noinput 
```

or 

```
$ mkdir -p app/cyclab/static/admin/
```

## API Docs
