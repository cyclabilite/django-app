# DOCUMENTATION

## API endpoints

| Endpoint | JS | Serializer |
| -------- | -- | ---------- |
| /api/cities | (GET) app.js when DOM is loaded | CitySerializer |
| /api/segments | (GET) entity.js when a segment is clicked | SegmentsSerializer |
| /api/segments/{segmentId} | (GET) entity.js when a vote has been made | SegmentsSerializer |
| /api/votes | (POST) entity.js to vote on a segment | VotesSerializer |
| /api/users/{userID} | (GET) user.js get user data of current user | UserSerializer |
| /api/votereasons | (GET) Call made in entity.js when app is loaded | VoteReasonSerializer |


## Procedures
### Mise au frigo

Deleted segments can be put in the fridge manually or automatically, so as to not have to transfer the votes immediately.

**Manual procedure** : in admin section click on deleted segment and set status *inFrigo* to True.

**Automatic procedure** : in the terminal run ->
```docker-compose exec app bash```
then
```python manage.py frigo```

The automatic procedure will set all deleted segments with a status *inFrigo=False* to True  and it will add the current *datetime* on which the segments was placed in the fridge.

### Vote transfer (manual)

It is possible to transfer the votes of deleted segments onto existing segments manually.
For this go to the admin section, click on a deleted segment and then *transfer votes*.
All the votes of the deleted segments will be displayed and the user can select new segments on which to transfer the votes.

This will call into action the following views:
 - **vote_transfer**: to render the vote information, deleted segments, and suggested new segments.
 - **handle_vote_transfer**: to transfer the votes to the newly chosen segments.


